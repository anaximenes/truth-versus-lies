# CHAPTER III

About June, 1952, my family and I moved to the suburb of
Evergreen Park. ¹ If my parents made the move in order to
provide me with "some decent kids to play with," they did
not choose the location well. The only kid in my age group
on our block was B.O., who was about a year younger than I
was. He was a frequent playmate of mine for one or two years
after we came to Evergreen Park, but he was a rather
obnoxious character and we didn't get along well. We had
several fights, all of which I won. A few years later, after
the O.'s had moved away, my mother told me she'd heard
that B.O. had gotten into trouble with the police, but, in
view of my mother's unreliability, I don't know whether
this is true.

Shortly after we arrived in Evergreen Park, my parents, in
order to encourage me to be socially active, made me enroll
in a summer program of organized recreation that was
conducted at Evergreen Park Central School. I didn't like
it, and soon stopped attending. At some later time my father
forced me to enroll briefly in the Boy Scouts, and I didn't
like that any better. I wrote in my 1979 autobiography, "As
a kid I usually didn't like activities that were organized
and supervised by adults, other than my parents." ²
Apparently this is typical for mathematically gifted kids.
According to a book on the psychology of adolescence, "An
interesting characteristic of mathematically gifted
adolescents was their independence with regard to how they
spent their out-of-class time. 'Though they played some
individual sports and some musical instruments, they
completely resisted any regimented activity in the way of
planned recreation.'" ³

In September, 1952, I entered the fifth grade at Evergreen
Park Central School. At Sherman School we had spent the
whole school day in one classroom and with one teacher, but
at Evergreen Park Central, the students shifted from one
classroom to another to be taught different subjects.
Because of this new system and the unfamiliar people I felt
very insecure at first, but after a few weeks I adjusted
comfortably. ²

I made some friends at school, including Dale J., Bob C.,
Barbara B., Dale Eickelman, and Larry S. Larry S. was the
best of these. The friendships with Dale J. and Bob C.
didn't last; the former turned out to be decidedly
peculiar, and the latter was a boy with little self-control
who once tried (unsuccessfully) to get me to participate in
stealing. Dale Eickelman had a few peculiarities of his own,
and I can't say that I ever really liked him, but I
continued to associate with him throughout my grade-school
and high-school years. My friendship with Barbara B. had
nothing to do with sex. Her family moved away before we
completed fifth grade, and thereafter I corresponded with
her for a short time. ⁴

Also in fifth grade, I carried on an intense flirtation with
a beautiful female classmate named Darlene Cy. Because she
teased me and provoked me, I loved her and hated her at the
same time. She gradually began to conquer me, however, and
love undoubtedly would have won out in the end if
circumstances hadn't separated us. What happened was that
upon completing fifth grade I was placed directly in
seventh, and after that I rarely saw Darlene. ⁵

Skipping a grade was a disaster for me. It came about as
follows. While I was in fifth grade the school guidance
counselor, Miss Vera Frye, gave some of us a battery of
tests including a Stanford-Binet IQ test. On the latter, I
scored very high, ⁶ 167. The *Washington Post* quoted my
mother as follows:

"A school psychologist \[Miss Frye\] gave Ted a
Stanford-Binet IQ test... . But his mother took more
comfort in the results of a personality test, which showed
him to be well-adjusted.

"'For a while \[Wanda said\] all my uneasiness about these
residual effects from his early childhood were laid to rest
because this psychologist said, "Oh, he is fine,"

... . In fact, she said he had a strong sense of security,
which surprised me... . She said he could be whatever he
wanted to be. ... He was the cat's whiskers.' ...

"\[The family\] now believe that perhaps Ted was smart
enough to figure out the most appropriate answers to the
test and outwit it." ⁷

Psychological tests include devices to detect cheating, and
it is hardly likely that a ten-year-old (however bright)
with no knowledge of psychological testing would be able to
outwit such a test.

In any case, Miss Frye telephoned my parents, informed them
of my high IQ score, and (according to my mother's account)
went so far as to tell them that I had the potential to be
"another Einstein." ⁸ This was foolish, because there is a
lot more to being an Einstein than scoring high on an IQ
test. It's possible that Miss Frye may have been laying it
on thick because she had previously encountered parents who
had shrugged their shoulders at information about their
children's IQ scores and she was therefore trying to
impress my parents with the importance of what she had to
say. If she had known something about my mother, she would
have been much more cautious.

My mother came from a very poor background - poor not only
financially but in every other respect. ⁹ Her position at
the bottom of the social scale had been very painful to her,
and she saw academic achievement, much more than financial
success, as the avenue to the social status that she craved.
She had neither the intelligence nor the self-discipline to
achieve anything herself, however, so she sought to fulfill
her ambitions through her children. ¹⁰ During my early years
her expectations were reasonable and she put only very
moderate pressure on me to perform well in school, but from
the time of Miss Frye's phone call, she was filled with
grandiose fantasies of what I was supposed to achieve.

Even at that time I felt that my mother's reaction to Miss
Frye's call was childish. Her excessive exhibitions of
pleasure seemed ridiculous, and she immediately telephoned
some of our relatives in order to brag to them. She told me
a great deal that Miss Frye had asked her to keep secret
from me. She admonished me not to reveal these things to
anyone, because "Miss Frye says we're not supposed to tell
you; but we feel that we can treat you as an adult." It was
from this time that I gradually began to lose respect for my
parents. ¹¹

It was essentially Miss Frye who decided that I should skip
a grade. She had the consent of the school authorities and
the enthusiastic support of my mother, but they relied on
her judgement as the supposed expert. Why did she make that
decision? My mother told me at the time that it was because
the tests showed that my greatest ability lay in the area of
mathematics and physics, and (supposedly) mathematicians and
physicists burned out young. Hence they were to be educated
rapidly so that maximum use could be made of their ability
while it lasted.

Many years afterward, in a discussion with my mother, I
bitterly criticized the decision to put me in seventh grade.
At that time she tried to justify the decision by claiming
that Miss Frye had said I was drawing "violent" pictures
during my free time in school, and that pushing me a year
ahead was somehow supposed to cure me of this. ¹² The
proposition that academic acceleration will cure anyone of
violent fantasies seems dubious, to say the least. Anyway, I
replied to my mother that drawing war pictures and the like
was commonplace among boys of that age at that time and
place, but she insisted that no, my drawings were different.
¹³ I brought the subject up again in 1991 in a letter to my
mother: "You claim that Miss Frye said I was drawing
pictures of violence during my spare moments in school. ...
I'm not aware that I drew violent pictures any more often
than the other boys. Miss Frye may have thought I did, but
I certainly wouldn't trust her judgement... ." ¹⁴ My
mother now changed her story. She wrote: "\[Y\]our memory of
Frye is faulty. She considered your drawings quite normal.
Just drawings of battle scene strategy." ¹⁵ This is a
typical example of the way my mother plays fast and loose
with the truth in order to suit her purposes of the moment.

Was I drawing abnormally violent pictures at the age of
ten? All I can say is that I do not remember making any
drawings that would be considered unusual for a ten-
year-old boy. ¹⁶ And my mother's statement quoted above,
that Miss Frye considered me "well-adjusted," weighs against
the abnormal-drawing story (assuming, of course, that my
mother's statement is true, which may not be the case).

\*\*\*\*\*\*

It was from the time I skipped a grade that I began to have
serious problems with social adjustment. I was not accepted
by the seventh-graders with whom I was put. I quickly slid
down to near the bottom of the pecking-order, and I stayed
there until I graduated from high school. I was often
subjected to insults or other indignities by the dominant
boys. ¹⁷ My attempts to make advances to girls had such
humiliating results that for many years afterward, even
until after the age of thirty, I found it excruciatingly
difficult - almost impossible - to make advances to
women. ¹⁸

Investigators working for my defense team obtained the
following information from Michael Johnson, an administrator
at Evergreen Park Community High School.

"Johnson... flatly declared that the experiment of
skipping kids ahead grades was a huge failure. The
experiment was a notable failure during the era that Ted
Kaczynski was promoted. Johnson added that the experiment
was most especially a disaster with boys and indicated that
he could document the fact that many of the boys who had
been skipped ahead during Ted's era ended up as
outcasts. ... Less-bright kids become resentful of those
boys who are advanced ahead, causing the smart and
accelerated kids to be even more acutely ostracized from
their peer groups. More important, Johnson added, girls do
not go out with boys who are younger. Thus, these boys have
been set up for failure, and fail they do. The act of
pushing youngsters ahead is almost never done anymore as a
result of these past experiments. In fact, the state of
Illinois now requires kids to be older before they can be
promoted ahead a year." ¹⁹

I was not the only kid who was rejected for being smart.
There were several other boys who had a reputation for being
academically-oriented and as a result were harassed or
treated with contempt by the "tough" kids. ²⁰ But in my
case the problems were compounded by the fact that, during
the same period, I was being subjected to psychological
abuse by both my parents. ²¹

I've already described the change in my mother's
personality that began not long after my brother's birth.
By the time I was in my teens, she was having frequent
outbursts of rage during which her face would become
contorted and she would wave her clenched fists while
unleashing a stream of unrestrained verbal abuse. ²² Even
when she wasn't having one of her outbursts, she was often
very irritable and would scold or make vicious remarks at
the slightest provocation.

The change in my mother affected my father. He became morose
and pessimistic, and when family squabbles arose, he tended
to sit in his easy chair and retreat behind a newspaper or
book, ignoring the sordid turmoil around him. ²³ Sometimes,
however, his patience became exhausted and he would have
angry arguments with my mother or with me.

But my father's moroseness was not exclusively an outcome
of the family situation. I believe that he had deep-lying
negative feelings about himself, about people, and about
life in general. When he was in his mid-sixties and more
ready to express his feelings than he'd been when he was
younger, he took a car-camping trip by himself. On returning
he said, "I can't be alone, because I don't like
myself." He tended to see other people as dirty or sick.
For example, when I visited my parents in 1978, my father
described his employer, Win PI., to me as a pathologically
compulsive talker. Later I got to know Win PI. myself, and I
found that he was rather talkative, but by no means
abnormally so. My father also used to speak of some of our
relatives and other people in terms that exaggerated their
failings and portrayed them as sick or repellent.

Throughout my teens I was the target of frequent verbal
aggression (often unprovoked) from both my parents,
especially my mother. ²¹ The insults that cut me deepest
were the imputations of mental illness or gross immaturity.
²⁴ I think it was my father who started these when I was
about twelve years old. The rejection I experienced from my
peers at school, in combination with the deteriorating
family atmosphere, made me often sullen and cranky, ²⁵ and
my father, characteristically, interpreted this in terms of
psychopathology. He began calling me "sick" whenever he
was annoyed with me. My mother imitated him in this respect,
and from then on until I was about 21 years old, both my
parents would apply to me such epithets as "sick",
"immature", "emotionally disturbed," "creep," "mind
of a two-year-old," or "another Walter T." ²¹ (Walter T.
was a man we knew who ended up in a mental institution.) It
was always in an outburst of anger that my mother called me
these things, but my father sometimes did so in a tone of
cold contempt that cut worse than my mother's angry
shouting. ²⁶ Neither of my parents ever suggested that I
should be examined by a psychologist or psychiatrist. ²⁷ My
mother never actually thought that there was anything wrong
with me mentally, and I doubt that my father saw me as any
sicker than he saw many other people. ²⁸ In saying cruel
things to me my parents were only using me as a butt on
which to take out their own frustrations. ²⁹

Though the imputations of mental illness were what hurt me
most, they comprised only a small part of the constant
verbal bullying to which I was subjected day in and day out.
My mother was continually shouting, scolding, insulting, and
blaming me for everything that went wrong, regardless of
whether I could have been responsible for it. During the
summer before I entered Harvard, she made an appointment for
me to see a professional photographer for a picture that the
university wanted for its records. When the day of the
appointment arrived, as it happened, I had a pimple on the
end of my nose. My mother angrily scolded me for it. "Look
at you! Now you've got a pimple on your nose! You're going
to look terrible in your Harvard photo! ..." And on and
on, as if it were my fault that I had a pimple.

In another case my mother drove me and some other members of
the high-school band to a music lesson. On the way back,
the other boys, who were older than I was, talked a good
deal about cars and driving. It made me feel small, since I
was still too young to drive. After she dropped the other
boys off, my mother began scolding me angrily: "Why don't
you get a driver's license like the other kids so I won't
have to be driving you all over the place all the time?" I
quietly pointed out that I was only fifteen years old and
couldn't get a license until I was sixteen. Instead of
acknowledging that she was wrong and apologizing, my mother
answered in an angry tone, "Well then, get a license as
soon as you are sixteen! ... \[etc.\]"

Once when I made a negative remark about someone's
competence, my father answered in a cold and sneering tone,
"You'll never be half as competent as he is." My father
did not typically lose his temper openly. Yet he sometimes
did so; in a few cases, he shouted at me, "I'll smash your
face!" I didn't believe he would really smash my face, but
still it was frightening to hear him say that.

These are only a few examples of the kinds of things that
went on constantly.

Physical abuse was minimal, but there was a little of it. A
couple of times my father threw me on the floor in the
course of family squabbles. My mother occasionally would
flail at me with her fists, but by that time I was old
enough (and my mother was weak enough) so that she didn't
hurt me.

Contrary to what my mother and brother have told the media,
up to the age of seventeen or so I was not socially
isolated. Throughout my grade-school and high-school years
I had several friends at all times. ³⁰ Though I was not
accepted by most of the seventh-graders with whom I was put
when I skipped a grade, I continued to associate with some
of the friends and acquaintances I'd made in fifth grade.
For example, Larry S. was a patrol-boy, and I used to stand
on his corner with him during the lunch hour; and I
continued to associate with Dale Eickelman ³¹ until I
finished high school. Moreover, I soon began to make friends
among the boys in my own grade; ³² but most of these friends
had low status among the other boys, ³³ and some of them,
like me, had a reputation as "brains" and for that reason
were subjected to insults and indignities. On the other
hand, one of my best friends had below-average intelligence.
³⁴ Apart from those already mentioned, a list of my friends
from seventh grade through high school would include Bob Pe.,
³⁵ Tom Kn., ³⁶ Jerry U., ³⁷ and G.Da. ³⁷ I hung around with
Russell Mosny ³⁵ quite a bit, but I never liked him much. We
tended to be thrown together because we were in many of the
same classes and were both "brains" who were treated with
contempt by the "tough" kids. Both Mosny and G. Da.
seemed to become cool toward me during my last year or so of
high school, ³⁸ but at the same time I became closer to Bob
Pe. and Tom Kn., and I made a new friend, Terry L. ³⁹

Having these friends, however, by no means compensated me
for the pain of the humiliatingly low status I had in
school. I skipped my junior year in high school, ⁴⁰ and
after that I was with kids who were two years older than I
was. Most of these kids didn't insult me, but they treated
me with condescension, ⁴¹ which was perhaps worse, and, with
the exception of Terry L., none of them had any interest in
making friends with me.

Even though I had friends, I spent a good deal of time
alone. By the time I was in high school, B.O. had moved away
and four other boys in my age-group had moved into our
block. One of these was simply a jerk. The other three, the
Tr. boys, were jocks and belonged to the "set" in school
by which I was intimidated; and moreover I had little in
common with them. With the exception of Bob Pe., all of my
friends lived far enough away so that visiting was
inconvenient, and consequently we went to each other's
homes only occasionally. Our activities tended to consist of
aimless time-killing. We rarely engaged in athletics apart
from occasional games of catch, we never undertook any
significant joint projects, we never attended any social
functions together. As I've already noted, most of my
friends had low status, and, while I was in school with
them, none was very active socially and none had
girlfriends. If they ever dated, they never mentioned it to
me.

The only serious activity I had was trombone-playing; my
music lessons brought me into contact with one of the very
few adults I knew at the time whom I really respected, my
teacher, Jaroslav Cimera. Two of my friends, Tom Kn. and
Jerry U., also played the trombone, and I often played duets
with one or the other of them.

Still, until I went to Harvard, my adolescence tended to be
an alternation among different kinds of boredom: A boring
day in school, a boring visit with a friend, a few boring
hours piddling around in my attic room, another boring day
in school. This doesn't mean that I never had fun with my
friends or alone, but that boredom was a nagging problem for
me. ⁴²

\*\*\*\*\*\*

Now let's look at the way my brother and mother have
portrayed me and our family life during this period. First,
the entries in my medical records that are evidently based
on my mother's statements to the doctors ⁴³:

"April 24, 1952 ... Appetite, activity and general
adjustment are all quite good."

"April 17, 1953 ... He eats well, plays actively,
presents no behavior problems."

"April 27, 1954 ... Now in 7th Grade and does well. Does
well socially."

"April 14, 1955 ... Eighth grade. Good grades. Active in
some sports. No further \[?\] problem except for some
adolescent \[illegible\]"

"April 20, 1956 ... He does very well at school - not
too much of a socializer, but is known as a 'brain'.
Gets along well with others when he tries - seems popular
but a little aloof."

"June \[?\], 1957 ... Accelerated in high school and will
finish next spring by going to summer school. Has his eye on
Harvard and \[illegible\] in physics and math.

"Health has been good but mother is concerned lest program
be too strenuous for him. Appetite good. Not very much
physical activity. No great interest in girls as yet."

"April 21, 1958 ... Ted has been well during the past
year. No problems. Is doing very well at school ... ."

The reason why my mother gave the doctors a rosy picture of
my adjustment (with barely a hint of social difficulties in
the April 20, 1956 entry) is that she has always been
extremely concerned with respectability ⁴⁴ and with
presenting to the world an attractive picture of our family,
and to this end she does not hesitate to lie.

In response to a request from Harvard, during the summer
before I entered college she wrote a long (two single-spaced
pages) letter in which she described my personality. In it
she gives a fairy-tale portrait of me as a budding
intellectual. For example, she speaks of my "serious
goals" and "ivory-towerish intellectuality," when in
reality I didn't have any clear goals at all and had little
respect for intellectualism. In fairness to my mother, I
should mention that in this letter she probably was not
lying calculatedly. She talked herself into believing all
that crap before she wrote it down and sent it to Harvard.
Her capacity for self-deception is remarkable. What is
significant for us here, though, is the way she described my
psychological and social adjustment:

"Ted is strong, stable, and has an excellent capacity for
self-discipline. However, I feel that he may be lonlier
\[sic\] than most boys the first few months away from home.

"... Ted does not respond quickly to friendly overtures.
He is pleasant and polite, but reserved; and accepts only an
occasional individual as a friend. Once he does, however,
the relationship is permanent. All of his friends share at
least one of his strong interests. One of these friendships
is based on a mutual fondness for exploring the countryside
and searching for fossils, arrowheads, and unusual rocks.
... He meets with another couple of friends because of a
shared appreciation for listening to and making music... .
Ted is also very fond of another boy who shares with him a
love for intellectual sparring, witty exchange and endless
polemics. The written and verbal communication of satire
and analysis on innumerable subjects by these two boys would
fill a volume. \[My mother has surpassed herself here. The
two musical friends must have been Tom Kn. and Jerry U., but
I have no idea who the other two friends could have been.\]

"The fact that he takes so little initiative in finding
friends, that he accepts the advances of so few people, ⁴⁵
and makes no attempt to join social groups makes us worry
about the possibility of his being a pretty lonely boy (from
our point of view - he claims he never feels lonely
because there is so much to do.) ⁴⁶ ...

"\[Ted\] has, as his counselor and teachers have said 'a
delightful personality, very witty and very clever.' ...

"... \[Ted is\] working successfully as a busboy this
summer and being well-accepted by the other people working
there. ⁴⁷

"One of the things that Ted's counselor hoped he would
learn to do was bring 'his light out from under the gushel
\[sic; "bushel" is meant\]'. He has always functioned
naturally and creatively ... almost devoid of the desire
to impress or communicate. ...

Perhaps the poor quality of the school and neighborhood
enviornment \[sic\] of his first ten years had something to
do with this. Looking back, we realize how little
stimulation and understanding he found there. Our own
confusion, uncertainty, and worry about his ever-increasing
propensity for solitary play didn't help matters. The
high-school counselor feels that Ted should become
increasingly aware of the desireability \[sic\] of
projecting his 'brilliance and wit.' More often now, he
will be placed in situations in which a stranger may want to
assess his talents in half an hour's time. His whole future
may depend on his ability and awareness of the need to
project himself at will at a particular time." ⁴⁸

Contrast the foregoing with my mother's portrayal of me in
her interviews with the *Washington Post* ⁴⁹ and on *60
Minutes*, ⁵⁰ in which she depicted me as severely disturbed
and almost completely isolated socially. You can believe one
version or the other, if you like, but you can't believe
both, since they are clearly inconsistent. Thus my mother
is again shown to be a liar. For present purposes it is
beside the point whether she lies calculatedly or talks
herself into believing her own crap before she tells it to
others.

It is true, though, that my mother may not have realized the
full extent of the social difficulties that I encountered
from the time I skipped sixth grade. I said nothing to my
parents about those difficulties because in our family
talking about personal problems, particularly on the part of
my brother and me, was almost taboo. ⁵¹ This was especially
true in my case, because, ever since Miss Frye had told her
about my high IQ score, my mother expected me to be her
perfect little genius. If ever I revealed to her any
failure, any weakness, it disappointed her and consequently
her response was cold and critical. ⁵²

\*\*\*\*\*\*

According to the *New York Times*, my brother described our
father as "always generous." ⁵³ In his interview with the
*Sacramento Bee*, my brother asserted that both our parents
were "warm and nurturing." ⁵⁴ According to the transcript
of the *60 Minutes* interview of my brother and my mother,
Lesley Stahl stated:

"Ted's fantasies, his family says, included accusations
that his parents had verbally abused and rejected him;
accusations that became more and more bizarre." ⁵⁵

Later in the interview, my brother said:

"\[Ted's\] feelings about our family bear no relationship
to the reality of the family life that we experienced. These
were loving, supportive parents." ⁵⁶

But here is what my brother told the FBI, according to the
latter's "302" reports of interviews with him:

"The relationship between TED Sr. \[Theodore R. Kaczynski,
my father\] and TED was mostly difficult and conflicted,
... DAVE remembers specifically that his father often told
TED, 'You're just like WALTER,' identifying WALTER as a
co-worker of his father's at the sausage factory who was
diagnosed schizophrenic. His father would often tell TED
'you have the mind of a two year-old.' DAVE remembered a
specific incident when TED ran to his father saying, 'Give
me a kiss,' and was rebuffed; TED Sr. pushed him away and
said, 'You're just like a little girl, always wanting to
kiss.' TED eventually 'got his kiss,' DAVE said, but he
never remembered that TED asked his father ever again for
affection. TED became increasingly reclusive, and quarrelled
constantly with his mother. TED Sr.'s behavior toward his
oldest son became increasingly cold and distant, and he
'mostly showed his disapproval' concerning TED." ⁵⁷

"Family members often ridiculed TED when they compared TED
with DAVE who was well liked because he had better social
skills." ⁵⁸ \[False; I was not "ridiculed" for this.\]

"DAVE noted that despite WANDA's concerns that certain
actions she and her husband took during TEDs childhood must
have been at least partly responsible for TED's lifelong
problems and isolation, WANDA is defensive of her own
actions in general, and sees herself as having unfairly
carried the main burdens of both her family of origin and
her own family. DAVE characterized his mother as 'often
difficult herself,' ..." ⁵⁹

Thus, my brother is clearly shown to be a liar. It's true
that the FBI's "302" reports often have inaccuracies, and
that the foregoing passages contain significant errors.
(Whether the errors originated with the FBI or with my
brother is an open question.) But it is hardly likely that
the FBI would just make all this up out of nothing; and, as
a matter of fact, much of it is corroborated by my
autobiographies and by family correspondence. ⁶⁰

In my 1979 autobiography, I wrote:

"One day, when I might have been about 6 years old, my
mother, father, and I were all set to go out somewhere. I
was in a joyful mood. I ran up to my father and announced
that I wanted to kiss him. He said, 'You're like a little
girl, always wanting to kiss.' I immediately turned cold
and drew back resentfully. My father immediately regretted
what he had done and said, 'Oh, that's alright. You can
kiss if you want to.' But there was no warmth in his voice.
Of course, I didn't kiss him then. ..." ⁶¹

This agrees fairly well with the account in the FBI report;
but notice that the incident occurred when I was about six
years old - before my brother's birth. Thus the FBI
report's implication that my brother personally witnessed
this incident is false. My 1979 autobiography continues:

"But the reader should be careful not to get an exaggerated
idea of the coldness that my father occasionally
exhibited - generally speaking I felt I had a good
relationship with my parents that didn't show any serious
deterioration until I was about 11 years old." ⁶²

My father did become rather cold toward me during my teens,
though my brother's account, as reported by the FBI,
somewhat overstates the case. I wrote in my 1979
autobiography, referring to my teen years:

"\[M\]y father tended to be cold. During my middle teens I
felt there was an undercurrent of scorn in his attitude
toward me." ⁶³

My brother and my mother state (more-or-less correctly)
that, during my adolescence, when visitors arrived at our
house, I would often retreat to my room. ⁶⁴ Thus they
unwittingly revealed information that helps to confirm the
abuse: According to investigators who have experience with
cases that involve child abuse, withdrawing from visitors is
a common reaction of abused children. ⁶⁵

## NOTES TO CHAPTER III

1. (Ab) Autobiog of TJK 1959, p. 3; (Ac) Autobiog of TJK
1979, p. 23; (Ga) Deeds #2, #3.

2. (Ac) Autobiog of TJK 1979, p. 23.

3. Karl C. Garrison, *Psychology of Adolescence*, 6th
Edition, Prentice-Hall, pp. 199, 200.

4. (Ac) Autobiog of TJK 1979, p. 23 states: "I had a few
friends in school, especially Larry S\_\_\_\_... ." Dale
Eickelman is discussed on pp. 50-52 of (Ac) Autobiog of
TJK 1979. In (Qb) Written Investigator Report #34, Eickelman
confirmed his friendship with me. None of the other
friendships is documented; for them I rely on memory.

5. (Ac) Autobiog of TJK 1979, pp. 47-50; (Ba) Journals of
TJK, Series VI #1, pp. 25-30 (October 1, 1974).

6. (Fa) School Records of TJK, E. P. Elementary; (Ab)
Autobiog of TJK 1959, p. 3; (Ac) Autobiog of TJK 1979, p. 24.

7. (Hb) *Washington Post*, June 16, 1996, p. A20.
Unfortunately, the results of the personality test are not
found in my surviving school records. That I did take such a
test is confirmed by (Aa) Autobiog of TJK 1958: "\[In fifth
grade\] I came to the attention of the curriculum and
guidance counselor... . I was taken out of class several
times that year to take a battery of tests, including I.Q.,
achievement, personality and aptitude tests."

8. (Ac) Autobiog of TJK 1979, p. 24.

9. (Ae) Autobiog of Wanda (the entire document).

10. (Ac) Autobiog of TJK 1979, pp. 78, 79; (Da) Ralph
Meister's Declaration, p. 1, paragraph 5; p. 2, paragraphs
7, 8.

11. Regarding the last sentence of this paragraph, see (Ac)
Autobiog of TJK 1979, p. 39; for all the rest of the
paragraph, see same document, p. 24.

12. (Ac) Autobiog of TJK 1979, pp. 24, 25; (Ca) FL #458,
letter from me to my mother, July 5, 1991, p. 10; (Cc) Notes
on Family Letters, Number 3 (written in 1991), p. 5.

13. (Cc) Notes on Family Letters, Number 3 (written in
1991), p. 5.

14. (Ca) FL #458, letter from me to my mother, July 5, 1991,
p. 10.

15. (Ca) FL#459, letter from my mother to me, July 12, 1991,
pp. 1, 2.

16. (Ac) Autobiog of TJK 1979, p. 25: "\[M\]any of the
other boys drew warlike or gruesome pictures. Whether I drew
such pictures more frequently than the other boys is a point
on which my memory does not enlighten me."

17. (Ab) Autobiog of TJK 1959, pp. 3, 4; (Ac) Autobiog of
TJK 1979, pp. 25-29; (Ad) Autobiog of TJK 1988, pp. 2, 3;
(Ca) FL #458, letter from me to my mother, July 5, 1991, pp.
10-12. In (Ab) Autobiog of TJK 1959 I greatly understated
the humiliations to which I had been subjected in school
because I was profoundly ashamed of them.

    The abuse I suffered in school was mostly psychological,
but there was a small amount of physical abuse. (Ac)
Autobiog of TJK 1979, p. 28:

    "\[A\] certain fellow verbally abused me, kicked my leg,
and kicked my briefcase - all for no apparent reason."

    (Ac) Autobiog of TJK 1979, p. 26:

    "\[In gym class\] a large, heavy boy intentionally ran
into me during a game, knocked me down, and fell on me,
bruising my arm very painfully."

    The injury was severe enough so that my parents took me
to the hospital and had my arm examined to make sure that it
wasn't broken. (Ea) Med Records of TJK, U. Chi., September
21, 1956, pp. 69-71. The "large, heavy boy" referred to was
Jack Mcl\_. When investigators working on my case tried to
track him down, they found that his last known address was a
transient hotel.

18. (Ab) Autobiog of TJK 1959, pp. 4, 14; (Ac) Autobiog of
TJK 1979, pp. 25, 52-55, 131; (Ad) Autobiog of TJK 1988,
pp. 2-4, 9, 11, 12; (Ca) FL #458, letter from me to my
mother, July 5, 1991, pp. 14, 15. Again, shame led me to
understate the case in (Ab) Autobiog of TJK 1959.

19. (Qb) Written Investigator Report #57, Michael Johnson.

20. Several former students at Evergreen Park Community High
School who were interviewed by investigators confirmed that
academically-oriented kids were harassed and insulted. These
included G. Da. (Qb) Written Investigator Report #28, pp.
1-3; Roger Podewell (Qb) Written Investigator Report #104,
pp. 1, 2; Wayne Tr. (Qb) Written Investigator Report #142,
p. 3. As I've indicated in the Introduction, information
reported to investigators about decades-old events has often
proved wildly inaccurate, especially when (as in this case)
there have been media reports that may have influenced it.
However, G. Da.'s reports of bitter personal experiences
should probably be given weight as showing the existence of
harassment, even though there is no way of knowing whether
the reports are accurate in detail.

21. (Ac) Autobiog of TJK 1979, pp. 40-42, 47; (Ca) FL #329,
letter from me to David Kaczynski, March 15, 1986, p. 2;
(Ad) Autobiog of TJK 1988, p. 3; (Ca) FL #458, letter from
me to my mother, July 5, 1991, pp. 5-8, 12; (Da) Ralph
Meister's Declaration, p. 3, paragraph 9. Further
documentation will be given in Chapter IV.

22. During October or November of 1996, Investigator #3 told
me that Dr. K. had told him that my brother had told her
that my mother would have outbursts of rage during which her
face would become red and contorted and she would make angry
gestures that frightened my brother. It is true that my
mother did have such outbursts, but I am relying on memory
for the fact that Investigator #3 made this statement to me,
since I did not write it down at the time.

    On August 14, 1997, I asked Dr. K. to confirm this, and
what she gave me then was a weaker version: "K asked, what
did you see when \[your mother\] was angry? \[Dave\] said:
Change of color in her face, her speech became quicker, she
might make sudden movements. K asked what he meant. He said
she would shake her hands and stomp her foot. As a child he
felt that it felt close to feeling what violence would feel
like - it was threatening." (Ra) Oral Report from Dr. K.,
August 14, 1997.

    I had the distinct impression from Dr. K. that "it felt
close to feeling what violence would feel like" was a
verbatim quote of my brother's words, and I clearly remember
that I asked her to repeat the sentence so that I could be
sure that I had it written down correctly. Nevertheless,
when I asked her for confirmation of this report on February
12, 1998, she gave me the following version, which seems
somewhat weaker: "Dr. K asked how did he know my mother was
angry. When she was very angry you could tell because her
color would change, speech would get quicker, would make
sudden movements, that one could imagine would be closer to
violence. Dr. K asked him what he meant. He said like shake
her hands and stomp her foot." (Ra) Oral Report from Dr. K.,
February 12, 1998.

    I asked Dr. K. about the words, "it felt close to
feeling what violence would feel like," and she said she
couldn't find them in her notes. If Dr. K. is asked about
this matter again, I have no idea what she will say.

    In any case, I know from my own memories that my mother
did have outbursts of rage as I've described.

23. (Ra) Oral Report from Dr. K., July 24, 1997: "Wanda
... Spoke of Ted R. withdrawing behind the newspaper. He
didn't like conflict, would withdraw from it and pick up
the paper." This is what Dr. K. told me, but, as I've
noted elsewhere, she sometimes changes her story or claims
she can't remember something she told me, so I do not know
whether she will confirm this information if she is asked.

24. (Ac) Autobiog of TJK 1979, pp. 40, 41.

25. Same, p. 42.

26. Same, p. 41.

27. This is confirmed in the interview with my mother in
(Hb) *Washington Post*, June 16, 1996, p. A20.

28. From (Ca) FL #330, letter from David Kaczynski to me,
late March or early April, 1986, p. 22:

    "I never, ever recall the parents berating you to me. In
fact, they always encouraged me to look up to you."

    My parents would hardly have encouraged my brother to
look up to me if they had thought I was the kind of sicko
that the media have portrayed with my mother's and brother's
encouragement.

29. (Ca) FL #458, letter from me to my mother, July 5, 1991,
pp. 3, 6.

30. (Ad) Autobiog of TJK 1988, p. 12. In (Ab) Autobiog of
TJK 1959, p. 11, I wrote: "My friendships are usually of
long duration. Fairly close, but never really intimate." I
was not aware of *any* really intimate friendships among the
boys in high school. The reader who thinks that there should
have been such friendships should bear in mind that the
teenage culture of Evergreen Park in 1955-58 may have been
quite different from what he is familiar with. Boys simply
did not bare their souls to one another.

    I went to Harvard at the age of sixteen and made no
close or lasting friendships there. However, during the
summer following my freshman year at Harvard I continued to
associate with some of my high-school friends ((Ac) Autobiog
of TJK 1979, p. 94; (Ab) Autobiog of TJK 1959, pp. 10, 11;
here, the "rather dull fellow" is Jerry U., the "large fat
fellow" is Russell Mosny, and the "very tall lank fellow" is
Bob Pe.). Consequently I date my social isolation from age
seventeen rather than sixteen.

    By the way, there is an error on p. 94 of (Ac) Autobiog
of TJK 1979. I wrote: "I think I became pretty well
separated from all my Evergreen Park friends within about a
year after leaving college." instead of "college", I should
have written "high school". I meant that I became separated
from these friends after the summer following my first year
at college. Actually, my memory of the chronology is rather
fuzzy here. It's possible that I may have continued to
associate with some of my high-school friends even during
the summer following my *second* year at college. In (Ac)
Autobiog of TJK 1979, pp. 94, 95, I may have inadvertently
telescoped the events of two summers into one.

31. (Ac) Autobiog of TJK 1979, pp. 50-54; (Qb) Written
Investigator Report #34, Dale Eickelman.

32. (Ab) Autobiog of TJK 1959, p. 3, referring to seventh
grade: "I did make a couple of good friends among the
better students... ."

33. (Ad) Autobiog of TJK 1988, p. 12.

34. (Ab) Autobiog of TJK 1959, p. 10: "One of my oldest
friends is a rather dull fellow, average intelligence...
." This was Jerry U. I was probably giving him a little
too much credit in describing his intelligence as average.

35. (Ac) Autobiog of TJK 1979, pp. 30, 94-95. Bob Pe. is the
"very tall lank fellow" described as one of my best friends
in (Ab) Autobiog of TJK 1959, p. 11. Bob Pe. confirmed his
friendship with me in (Qb) Written Investigator Report #100.

36. Ruth Kn., Tom's mother, has confirmed that he and I were
friends. (Qb) Written Investigator Report #64, p. 1. I
mention this report for whatever it may be worth, but some
of the other information given by Mrs. Kn. is incorrect.

37. (Ac) Autobiog of TJK 1979, p. 94.

38. (Ac) Autobiog of TJK 1979, pp. 25, 94, 119-121. Mosny is
the "large, fat fellow" referred to in (Ab) Autobiog of
TJK 1959, pp. 10, 11.

39. (Ac) Autobiog of TJK 1979, pp. 29, 94.

40. (Fb) School Records of TJK, E.P. High School; (Fc)
School Records of TJK, Harvard, pp. 12, 14; (Ac) Autobiog of
TJK 1979, p. 28; (Ab) Autobiog of TJK 1959, p. 4; (Aa)
Autobiog of TJK 1958, p. 2.

41. (Ac) Autobiog of TJK 1979, p. 28.

42. (Ac) Autobiog of TJK 1979, pp. 46, 47 has: "Throughout
my earlier teens I suffered increasingly from chronic
boredom... . Often I would visit a friend's home, or a
friend would visit my home. But if these visits lasted any
length of time, I would usually get bored... . Best, I
liked physical games such as playing catch; but...
outside of gym classes, I never had a chance to participate
in complicated games like softball and football, which I
suppose would have held my interest better. Because there
were never enough guys available for a regular game, we had
to play very simple games like catch."

43. (Ea) Med Records of TJK, U. Chi., April 24, 1952, p. 53;
April 17, 1953, p. 57; April 27, 1954, p. 58; April 14,
1955, p. 59; April 20, 1956, p. 67; June, 1957, p. 73; April
21, 1958, p. 74.

44. (Ac) Autobiog of TJK 1979, p. 79: "Respectability is
important to her."

45. I can think of few instances (prior to the time when my
mother wrote this letter) in which I intentionally rejected
friendly advances. No doubt I often seemed cool toward
people; this was because my experiences in school had
conditioned me to be afraid of social situations and of the
possibility of rejection. Moreover, one of the symptoms of
abuse is social withdrawal.

    From (Ac) Autobiog of TJK 1979, pp. 28, 29:

    "As a result of \[the rejection I'd experienced\] I
think I developed a kind of stoical coldness. (Not daring to
fight back, and not wishing to show weakness, my only choice
in the face of hostility was to be cold and stoical.) The
cold impression was often accentuated by shyness, and I
suspect that my apparent cold aloofness may have alienated
some kids who might otherwise have been friendly."

46. Actually I suffered from chronic boredom. See Note 42.

47. The truth: "\[M\]y parents put pressure on me to earn
money to help pay for my education... . I was supposed to be
not only brilliant, but industrious... .

"I felt very shy and uncomfortable among the people on
these jobs. When asked about my personal background I should
have lied. The first job I had the first summer was as a
busboy in a restaurant. One waitress there gave me a hard
time, being evidently jealous of my education; she would
bitterly make remarks like: 'We don't need brains around
here - we need a strong back.'" - (Ac) Autobiog of TJK
1979, p. 95.

48. (Fc) School Records of TJK, Harvard, pp. 18, 19; letter
from Wanda Kaczynski to Skiddy Von Stade (Harvard Dean of
Freshmen), July 16, 1958. I had already been admitted to
Harvard, so there was no need for my mother to fib in order
to secure my admission.

49. (Hb) *Washington Post*, June 16, 1996.

50. (He) *60 Minutes*, September 15, 1996.

51. My brother told Dr. K. that there was no "permission"
to talk with parents about internal struggles. (Ra) Oral
Reports from Dr. K., July 24, 1997 and February 12, 1998. As
noted elsewhere, oral reports I've received have not proved
reliable; but see Note 52.

52. (Ac) Autobiog of TJK 1979, p. 115; (Ca) FL #458, letter
from me to my mother, July 5, 1991, pp. 6, 7. (Da) Ralph
Meister's Declaration, p. 3, paragraph 10 has: "Teddy John
was... afraid to tell Wanda about emotional problems or
difficulties he encountered with his peer group because that
would have caused a rent in the picture she had of her
son."

53. (Ha) *NY Times Nat.*, May 26, 1996, p. 22, column 1.

54. (Hc) *Sacramento Bee*, January 19, 1997, p. A16,
column 1.

55. (He) *60 Minutes*, September 15, 1996, Part One, p. 8.

56. Same, Part Two, p. 3.

57. (Na) FBI 302 number 2, pp. 6, 7.

58. (Na) FBI 302 number 1, p. 3.

59. (Na) FBI 302 number 3, p. 5.

60. See Note 21 above. But contrary to what the FBI says my
brother told them, I was compared to Walter T. only twice,
and in at least one of those cases it was my mother who made
the comparison.

61. (Ac) Autobiog of TJK 1979, p. 18. The story is also told
in (Ca) FL #339, letter from me to David Kaczynski, Summer,
1986, p. 4. My brother probably got the story from this
letter and at some subsequent time began to imagine that he
had witnessed the incident himself.

62. (Ac) Autobiog of TJK 1979, pp. 18, 19.

63. (Ac) Autobiog of TJK 1979, p. 41. (Ca) FL #407, letter
from me to David Kaczynski, October 13, 1990, p. I has: ".
.. during my teens, but, while Dad was always rather cold
to me during that period ... ." Also see (Ca) FL #408,
letter from me to my mother, October 13, 1990 (copy kept in
cabin).

64. (He) *60 Minutes*, September 15, 1996, Part One, p. 3:

    "WANDA KACZYNSKI: ... if \[Ted\] heard cars driving up,
he'd say 'ooh, there's so-and-so.' He says, 'don't call me
down. I - I don't want to see them. I don't want to see
them.' He'd go upstairs."

    The foregoing is not strictly accurate, but it is true
that I often avoided visitors by going to my attic room. See
also (Hb) *Washington Post*, June 16, 1996, p. A20, middle
of last column on the page. And see (Na) FBI 302 number 1,
p. 3: "DAVE noted that TED would often retreat to the attic
whenever anyone came to the house to visit."

65. (Qc) Written Reports by Investigator #2, p. 2:
"Withdrawal is a common reaction for abused children and
includes withdrawing from visitors."
