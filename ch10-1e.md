# CHAPTER X

Let's begin with two media reports of the Ellen Tarmichael
affair. Following a paragraph that gave a badly garbled
account of how I came to work at Foam Cutting Engineers in
Addison, Illinois, the *New York Times* wrote:

"[Ted's] supervisor was Ellen Tarmichael, a soft-spoken
but no-nonsense woman who is still a production manager with
the company. One employee, Richard Johnson, called her 'a
wonderful boss, the best I've ever had,' and added:
'She's always kind-hearted and nice to people. I can see
why somebody would get interested in her.'

"Ted Kaczynski became interested in late July 1978. ...
[Actually it was mid-July or earlier. ¹]

"It was a Sunday, and he had gone for a walk. 'He happened
to see her car,' David recalled. 'She was filling the gas
tank. [This is not quite accurate. ²] I don't know exactly
what transpired. He actually went to her apartment and
played cards with her and her sister and her [sister's]
boyfriend.'

"Later Ted came home. 'He was obviously in a good mood,'
David said. 'He told me he had gone to see Ellen, that
they had spent the day together... and that some gestures
indicating affection had passed between them. I was very
happy about that.'...

"They had two dates, Ms. Tarmichael recalled. She said he
seemed intelligent and quiet, and she accepted a dinner
invitation in late July. It was a French restaurant, David
said, and Ted 'ordered wine and he smelled it [false: no
wine was ordered], he made a big deal of it.' David added,
'He had a good time.'

"Two weeks later, they went apple-picking and afterward
went to his parents' home and baked a pie. That was when
she told him she did not want to see him again 'I felt we
didn't have much in common besides our employment,' she
said. [This is no doubt true as far as it goes, but it is
only part of the truth and by no means the most important
part.]

" 'Ted did a total shutdown,' retreating into his room,
David said. He also wrote an insulting limerick about Ms.
Tarmichael, made copies and posted them in lavatories and on
walls around the factory. He did not sign the limerick, but
his relationship with the woman was known. [How?  I never
told anyone about it except my father, brother and mother.
It could have become known at the plant only through
blabbing by my father, by my brother, or by Ellen
herself.]

"David confronted his brother. 'I was very, very angry,'
he said. 'Part of me was disappointed. He was so close to
being integrated in the most primal rite of integration. He
had an interest in a member of the opposite sex, and to have
him go back to this kind of angry, inappropriate behavior -
to the family it was embarrassing, adolescent kind of
behavior.'

"David told him to cease the offensive conduct. But Ted put
the same limerick up the next day, above David's desk
[actually I put it on the machine he was working with].
David told him to go home. [That is, he fired me, which he
could do because he had become a foreman by that time.]...

"David said Ted wrote Ms. Tarmichael a letter that 'had
elements of apology about it.' But the investigators said
the letter, which probably was not sent [it *was*
sent - ³] partly blamed the woman for what had happened and
said Ted had considered harming her." ⁴

This is how the *Washington Post* described the affair:

"Sometime before June 23, 1978, Ted wrote saying he needed
money. They told him to come work with Dad and David at the
Foam Cutting Engineers Inc. plant." ⁵

Here is another one of those seemingly minor distortions
that the *Washington Post* no doubt will claim is accidental;
yet the slight misstatement seriously misrepresents what
actually happened, and, as is usual with the media's
misstatements, it tends to make me look bad. Readers will
of course interpret the *Washington Post's* statement to mean
that I wrote home asking for money and that my parents
told me that if I wanted it I would have to come
and work for it. In fact, I did not write my parents asking
for money; I asked, on my own initiative, whether it was
likely that I could get a job at Foam Cutting Engineers.
This is proved by the letters that I quoted in
Chapter VII, pp. 211, 212.

The *Washington Post* continues:

"Ted Sr. was a manager, and David was Ted\'s boss." ⁵

Both statements are false. My father was not a manager but a
sort of jack-of-all-trades who worked only part of the year
fixing the machines, building jigs, and troubleshooting
generally. David was the boss neither of Ted Sr. (my father)
nor of Ted Jr. (me). When I started at Foam Cutting
Engineers my brother was only an ordinary worker. Later he
was promoted to foreman of the evening shift - but I worked
on the day shift, so that he was not my boss. As I remember
it, the shifts overlapped to some extent; the evening shift
started at some time in the late afternoon before the day
shift left. That was why my brother and I were at work at
the same time and he had an opportunity to fire me. Since he
was not the foreman of my shift, I was in doubt about
whether he had the right to fire me, but Ellen confirmed the
firing. I'm not certain that I remember correctly the
overlapping of the shifts and the exact authority that my
brother had at the moment of the firing; but that my account
is approximately correct is confirmed by an entry in my
journal that was written on the very day of the firing:

"This afternoon, I went over to where my brother was
working, pasted up a copy of the poem before his eyes, and
said, 'OK, are you going to fire me?' Of course, he did.
Wanting to make sure that the firing was official (Dave is
night boss and I am on the Day crew) I went into Ellen's
office and asked her if the firing was official. ...[S]he
said that... she would have to uphold the firing." ⁶

To proceed with the *Washington Post* Article:

"Soon after he arrived at the family home, then in Lombard,
Ill., Ted had a date with a co-worker named Ellen
Tarmichael. Wanda and Ted Sr. were thrilled. After two
dates, Ellen lost interest. Ted, in a rage, posted insulting
limericks about her at the plant. David had to fire his own
brother, a predicament he now sees as 'foreshadowing what I
had to do later' in turning Ted in to the FBI. Ted locked
himself in his room for days." ⁵

The last sentence is at best misleading. All members of my
family took an angry and accusing attitude toward me after
the incident, and consequently, for the next two or perhaps
three days, when I was at home I spent most of my time in my
room rather than with the family - as I'm sure the majority
of people would have done under similar circumstances.
Most of the time my door was not locked. Within a few days
I went out and got another job. ⁷

The rest of the paragraph and the following two paragraphs
of the *Washington Post* article are wholly false and reflect
only my mother's inability to distinguish truth from her
own fantasies. The next paragraph refers to the letter that
I wrote Ellen Tarmichael on August 25, 1978 (the letter is
dated) and showed to my family by way of explanation either
on the 25th or the 26th:

"Ted came out of the room with several written pages in his
hand, his attempt to explain himself. He wrote that Ellen
had been intentionally cruel to him. None of them
[the family] felt that was even remotely true. [That's
false!] At the end of the missive,
he repeated his insulting limerick, said David, 'like he
wasn't going to take it back. No matter what.'" ⁵

This is either another lie or another error on my brother's
part. I saved a carbon copy of the letter, and the
insulting limerick is repeated nowhere in it. ⁸

\*\*\*\*\*\*

Now here is the full and true story of the Ellen Tarmichael
affair.

When I started work at Foam Cutting Engineers, Ellen was the
day shift foreman and therefore my immediate superior. At
first I did not find her sexually interesting because, while
her face was attractive, her figure was not. As I wrote in
my journal, "She has a beautiful face but a very mediocre
figure (too much fat on her ass thighs" ⁹ But as I got to
know her I found that she had a good sense of humor and was
apparently a very nice person; and, as I wrote, "she is
very attractive because she has *charm*, her personality, so
far as it is exhibited to the world at large, is very
attractive, she is apparently very intelligent, and probably
quite competent." ¹⁰ She seemed very friendly toward me
and, rightly or wrongly, I thought she liked me.

I'd had very little contact with women for several years,
and I'd had no relationship with one for fully sixteen
years, since I'd broken off with Ellen A. This rendered me
very susceptible, with the result that within two or three
weeks of starting at Foam Cutting Engineers I got infatuated
with Ellen Tarmichael - as my journal records. ¹⁰

As I explained in Chapter III, p. 83, ever since the painful
experiences of my adolescence I had found it extremely
difficult to make advances to women. In this case I found it
even more difficult than usual because Ellen, my father, my
brother, and I were all working at the same shop, so that,
if I made advances to her and was rejected, I would feel
shamed before my own family - who were not tolerant of any
weaknesses or mistakes on my part. I couldn't seem to get up
the nerve to ask her out either at work or by telephone, so
one Sunday afternoon I looked up her address and took a
stroll in that direction with the intention of making her an
unannounced visit, ¹¹ assuming I didn't chicken out, as I
probably would have done. But by sheer chance I happened to
meet Ellen along the way - at a gas station, though the
meeting was a bit more complicated than what my brother
described to the *New York Times*. ¹² She greeted me
cordially, I told her I'd been going to drop in on her, she
invited me into her car and "she drove me to the apartment
that she shares with her sister Liz. Liz was there with her
boyfriend George; but they shortly left to play golf so that
I had a pleasant conversation of 2 or 3 hours with Ellen.
She told me a good deal about herself... . [S]he has a
streak in her personality that would be attractive if it
were not so strongly developed; but as it is, I think it
repels me more than attracts me; it is a kind of egotistical
streak, or a need for superiority and dominance. You would
never guess from he[r] usual behavior that she has such a
streak; but she told me that when she was a kid (she was the
second child in the family) she had a tremendous need to do
better than her elder brother... in all activities
whatsoever. In every sport, in school, etc. She would
practice and practice a sport all by herself until she could
beat her brother. She claims she succeeded so well that she
thoroughly demoralized her poor brother. She says that up to
a couple of years ago she believed she could do anything.
She seems to be conceited about her job and overestimates
her importance to the company. She says she intends to be
president of the company some day. Yet she says all these
things in a gentle and feminine manner, not in a boastful or
aggressive way. ...  Liz and George had returned... we all
played pinochle until after 11 PM. ... [Ellen] drove me
home. When we arrived, I said, 'Am I being too aggressive if
I ask for a goodnight kiss?' She averted her eyes and moved
her head...as if she were hesitating. Then she said
'alright.' (I suspect she really had no hesitation about
kissing but was only trying to make a certain impression.)
Then she leaned over toward me for the kiss and we had a
nice big juicy delicious kiss with firm pressure. Now, I am
so very inexperienced in these matters that I am in a very
poor position to judge, but it did seem to me that she
kissed me somewhat aggressively; at least, she had her mouth
on mine before I was even ready for it. I said in a soft and
rather fervent tone, 'Oh, I like you!' She gave the curious
reply: 'You can't say that.  You don't know me.' then we
said goodbye. I didn't think much about her reply at the
time, but it seems particularly curious in view of a rumor
that my father told me about today: It is said that Ellen
never goes out with any man more than once or twice." ¹³

Actually, I had overheard my father telling my mother the
same thing a few days earlier; see below.

When I got home (i.e., to my parents' house) after my visit
with Ellen, I went to my brother's bedroom and told him
about my experiences of the day. He seemed oddly
unresponsive; he showed no emotion, said little, and asked
no questions.  I then said, "A few days ago I heard Dad
telling Ma that Female 16 says that Ellen goes out with a
guy a couple of times and then you never hear any more about
it. Have you heard anything about her?" My brother said he
had heard nothing definite, only that there was "something
funny" about Ellen in her relations with men. ¹⁴ The next
day I asked my father about her and he told me directly (as
indicated above) that it was rumored that she never went out
with any man more than a couple of times.

Before my visit with Ellen at her apartment she had been
invariably kind, obliging, and friendly toward me, but from
the time I showed that I had a sexual interest in her a
certain inconsistency manifested itself in her behavior
toward me. Now and again she would make a remark that had a
certain bite to it, not enough so that it could definitely
be called rude, but enough to make me wonder.

From my journal:

"July 29 [1978]. Yesterday I took Ellen Tarmichael to an
expensive restaurant for supper." ¹⁵

The table conversation was pleasant enough, except that
Ellen gave further
indications that she had an excessive concern with power,
and maybe even a sadistic streak:

"[S]he...said to me that she was a 'very vindictive
person' and would do anything 'no matter how underhanded'
to get revenge if she wanted it... ." ¹⁶

When we left the restaurant,

"[S]he...invited me to her apartment, where, she
hastened to add, we would not be alone. Actually we *were*
alone for an hour or more as her sister and sister\'s -
boyfriend were out-to eat. The situation was not such that
I could readily make any sexual advances... . After her
sister and sister's boyfriend returned I had a very boring
time listening to a conversation in which I took very little
part. Finally, at 12:30 AM, Ellen asked me if I would like
to 'go out for coffee.' I said yes. So I drove her to a
place nearby that she recommended. We spent an hour and a
half there discussing various topics. Then I took her home,
and, on arrival, asked for a goodnight kiss. I got an even
better one than last time. Mouths wide open, tongues
rubbing. *She* started that open-mouth, tongue-rubbing stuff,
not me. ... All this might have lasted, say, 3 minutes.
Then she said, 'I think it's time for you to go home.' So
I did. Though she is very charming and attractive much of
the time, by now I greatly dislike her because of her
egotism and its consequences; for example: she spent some
time bragging about how she was going to become president of
the company and how she was in on company secrets and so
forth... .

"... She says that Wynn \[sic; should be Win\] (the
president of this 2-bit foam-cutting corporation) likes me
and would like to keep me in the company, or at least is
thinking along those lines. She asked me not to tell Wynn
that I had gone out with her; because she said that Wynn had
suggested to her that she should use herself as bait to keep
me around the company; but she had refused. A couple of
hours later when this subject came up again, she said that
Wynn had only made the suggestion in jest. I don't know
just what the truth of the matter is; I wouldn't trust
Ellen for strict accuracy." ¹⁷

In spite of the fact that I wrote in my journal, "by now I
greatly dislike her," I was still infatuated with Ellen.
After our dinner date her behavior toward me became more
inconsistent than it had been before. At times she was warm
and friendly and seemed to invite my overtures; at other
times, for no apparent reason, she would turn so cold that
she seemed to be trying to hurt me.  Hence I told myself
repeatedly that I wasn't interested in her any more.
Undoubtedly I would really have lost interest in her if I
hadn't been so sex-starved, or if I had known how to look
elsewhere for a woman. As it was, I remained infatuated.

Without revealing the extent of my feelings toward Ellen or
the fact that she sometimes seemed to be hurting me
intentionally, I discussed with my father and brother her
egotistical and disagreeable concern with power. They agreed
that she did have such a concern, and my brother attributed
it to feelings of inferiority. I answered that I saw no
evidence of such feelings on her part.

On Sunday, August 20, I took Ellen out to the forest
preserves to pick wild apples, from which we were to make
pies. Three days later I wrote:

"It now seems clear that from the very beginning of this
date she was out to humiliate me, or at least to assert a
certain type of superiority over me. This in spite of the
fact that I had made it very clear to her that I was very
sweet on her. I was at pains on this date to be attentive
and agreeable; but she was very cool; not so much so as to
bring out any open disagreement, but just the right amount
to leave me unhappy and wondering." ¹⁸

For example, when we got out of the car at the forest
preserve, instead of walking alongside me, she walked a
couple of feet behind. Two or three times I waited
for her to catch up and tried to walk alongside of her,
but in each case she promptly dropped back again, though
I was walking slowly. ¹⁹ This was particularly
embarrassing to me since there were many people present at
this popular picnic spot. When we headed home with the
apples, she insisted that we should make the pies at my
parents' house, but that I should first take her back to
her apartment so that she could get her car and drive
herself to my parents' house, then drive herself home
afterward.

"She insisted on a peculiar way of using her auto and mine
[actually, my father's\]; this arrangement was such that I
would have no opportunity to ask for a goodnight kiss. At
this point I felt that explicit clarification was called
for, so I asked her if she was intentionally avoiding a
goodnight kiss. After a little hesitation she answered that
she was. I then asked further questions..." ²⁰

When I thus tried to open to the light of day her indirect
and half-covert maneuverings, she became quite tense, and
her voice was at first slightly shaky.

"...and what she told me was essentially this: She had no
sexual interest in me; she said she liked me, but the way
and the context in which she said it indicated that it was
the condescending sort of liking that one might have for a
child or for some other kind of social inferior.

"She claimed she went out with me mainly in order to
satisfy her curiosity about me because she had never met
anyone like me before. She said a kiss 'doesn't mean
anything.' She claimed there was no sex in it when she
kissed me. (This seems a little implausible in the case of
an open-mouth kiss with tongues rubbing... .)

"During the first part of the date she [had been] cool
and a little glum; but...after she had humiliated me she
immediately became quite cheerful and gay for the rest of
the day. ...

"It seemed to me that during the rest of the day she would
occasionally rub in her little triumph by making remarks
that were somewhat cutting but not so much so as to bring
about any open breach of friendlyness [sic]. For example,
I asked her what were some of my unusual characteristics
that made her feel I was 'unlike anyone she had ever met.'
The first one she mentioned was: 'You are so very lacking
in confidence socially.' (True enough, but not nice to say
so, unless after taking special pains to be tactful.)" ²¹

One thing she told me in the course of that conversation
particularly struck me. She talked about some fellow she had
gone out with a great deal when she was in college, saying,
"I treated him rotten, I even stood him up one time, but he
still kept taking me out." What was remarkable was the
*relish* with which she said she had "treated him rotten."

At the time, I was desperately confused about Ellen and her
behavior toward me, but after the dust had settled the
explanation seemed pretty clear. She, to my way of thinking,
was a sexual sadist. Under ordinary circumstances she was a
nice, friendly, considerate person; but when she was feeling
sexy she got her kicks from hurting men. ²² Probably most
men were not seriously hurt by her. After having a couple of
dates with her and learning what she was like, they just
stopped asking her out. But I was especially vulnerable
because of my past history and my inexperience with women.
During the latter part of that last date,

"I took pains to conceal my feelings, and remained
outwardly cheerful and friendly, though half the time I
wanted to cry and the other half the time I wanted to kill
her." ²³

"I loved that damn bitch. She knew I had soft feelings
toward her and she intentionally used these to lead me on
and then she calculatedly humiliated me.

"I was so upset by this that for the next 2 nights I was
unable to sleep more than 4 hours a night, and, what was
worse, I was exhausted by nervous tension. That date was
Sunday. Monday I did nothing about it because I was
exhausted and had had no time to think things over. But
after work I did think things over; I had an overwhelming
need for revenge and I decided to get it by persistently
needling and insulting her at work." ²⁴

I hoped I could bring matters to such a pass that the whole
nasty business would be dragged out in front of the crew,
presumably to Ellen's great embarrassment. ²⁵

"I started Tuesday morning by pasting up some copies of an
insulting poem that I wrote about her." ²⁶

"I don't doubt that I could have made things very
unpleasant for her by such methods - except that my
weak-minded, self-righteous brother took it upon himself to
interfere. Having seen the poem I pasted up, he said he
would fire me...and 'maybe bust your ass, too' if I did
it again." ²⁷

I asked my brother to listen to my side of the story, but he
angrily refused to do so, and let stand his threat to fire
me.

"Of course, that was a direct challenge, so I wasn't
about to back down. This afternoon [August 23, 1978], I
went over to where my brother was working and pasted up a
copy of the poem before his eyes...," ²⁸ whereupon he
fired me, as described earlier. When I went to Ellen's
office to ask her whether the firing was valid, she seemed
dismayed at the situation and was apparently reluctant to
confirm my dismissal. In my journal, naturally, I put a
negative interpretation on this behavior, ²⁹ but in
retrospect I think she was genuinely sorry at what had
happened. Despite her description of herself as
"vindictive" (see p. 283) I don't think she was in
reality a vindictive person under ordinary circumstances. I
think her sadistic streak manifested itself only when she
was feeling sexy; it was for her a source of sexual
gratification and did not imply any tendency to cruelty in a
non-sexual situation.

Since my brother had frustrated my retaliation against
Ellen, I was choking with anger, and, to make matters worse,
my mother and father turned against me too, *without*
listening to my side of the story first. ³⁰

"[T]hat weak fool Dave has made that bitch's triumph
complete: She humiliates me sexually, she gets me fired from
my job, and she causes dissension in my family.
I have shed more tears over that cheap whore than I have
over anything since my teens... .

"What makes this particularly hard is the fact that it
recalls bitter experiences over many years, reaching right
back to my early teens...," ³¹ namely, the rejections I had
experienced and my complete lack of success with women. I
was more choked with frustrated anger than I'd ever been in
my life, so I decided to retaliate against Ellen in the only
way that remained to me - by attacking her physically. To
abbreviate as much as possible the account of a distasteful
episode, on Thursday, August 24, I waited for Ellen in the
parking lot of Foam Cutting Engineers. When she arrived I
confronted her, talked with her briefly, and then left
without laying a finger on her. ³² After that my anger was
burned out, and since then I haven't hated her.

The next day I went out and got a job at Prince Castle (by
that time I'd learned how to lie on application forms), and
the same day I wrote Ellen a long letter of explanation,
which I *did* mail. According to the media, Ellen has said
that she never received "any correspondence" from me. ³³
If she did say that, then she was not telling the truth. A
letter is occasionally lost in the mail, but besides the
first letter I also sent her a second letter (dated
September 2, 1978), and the chance that *both* of these
letters could have been lost in the mail is so slight that
we can be for all practical purposes certain that she
received at least one of them. Both letters are reproduced
in Appendix 9.

Why has Ellen denied receiving my letter? Maybe she doesn't
remember it, or maybe she wants to avoid discussing its
content, which would force her to address the issue of her
behavior toward me.

Probably on August 25, when I wrote it, or conceivably on
the following day, I showed the letter to my parents as a
way of explaining my behavior. They read it and said that
now they understood better; the tension went out of the
atmosphere and we were reconciled. However, my parents did
not apologize for the way they'd reacted earlier. Then I
went to my brother's bedroom (where he spent most of his
time when staying at the house in Lombard ³⁴) and showed
him the letter. He too read it, and while he did not
apologize explicitly at that time, ³⁵ his manner seemed to
indicate that he regretted the way he had reacted; and I was
reconciled with him, too. The *New York Times* stated that
"tensions between the brothers continued]," ³⁶ but this
is false.

In fairness to Ellen Tarmichael I must make it clear that
when the whole affair was finished her attitude was
conciliatory and even kind. As I wrote in my journal:

"Sept. 1. Yesterday...my father brought home from
Foam-Cutting Eng. a present of home-made cookies from
Ellen, for the family. ...I sent Ellen a message through
my father: that the cookies were delicious, that I apologize
for the tone of my letter, and that I no longer have any
hard feelings toward her. Today he said he'd given her the
message. He said she seemed pleased and that she said: 'I
think the problem was that Ted and I speak different
languages.' " ³⁷

Notice that this passage tends to confirm that Ellen did
receive my letter. If she hadn't received it, then, when my
father told her that I apologized for the tone of the
letter, she presumably would have answered that she hadn't
received any letter, and my father would have reported that
fact to me.

Also notice that Ellen failed to face up to the real source
of the problem - that she had a streak of sexual sadism.

\*\*\*\*\*\*

The reader will please review my brother's recent remarks on
the Ellen Tarmichael affair as reported by the *New York
Times* and the *Washington Post* (quoted at the beginning of
this chapter) and compare them with the following passages
that he wrote in 1981, some three-years after the events:

"I was wrong to fire you and threaten you. I did so in
anger because you were behaving badly (which is your own
business) and because you caused severe embarrassment to Dad
and me. ... But I realized soon afterwards that I should
have taken into account how badly you were feeling at the
time." ³⁸

"I think if the manner of your taking revenge against Ellen
had arisen in its own isolation, I probably would have
responded very differently, though it would be impossible
now to know for sure. I hope, at any rate, that I would have
responded differently." ³⁹

There follows a passage in which my brother argues that
during the months preceding the incident in question I had
been treating our parents badly. It is a passage that I am
unable to understand, since it seems to me that during that
period my relations with our parents were better than at any
other time since I was eleven or twelve years old.

My brother's letter continues:

"When you brought trouble into the workplace (as I
conceived it) I guess I just lost my head and my discretion
completely. ... ⁴⁰ I say again that I was wrong to do what
I did, although I suppose I have learned (for whatever good
it will do me) how thoroughly I can be undone by my bad
temper. ... ⁴⁰ From my point of view, all of this is in
the past, though of course I acknowledge the major injury
was yours not mine." ⁴¹

These passages show that, while my conduct in the Tarmichael
affair was not exactly noble and generous, my brother did
realize that there were two sides to the story and that my
behavior was at any rate understandable (whict does not
imply that it was blameless). Yet, if the *New York Times*
and the *Washington Post* have reported his remarks
accurately, he gave them a one-sided version of the affair
that made it appear that there was no mitigation for my
behavior.

This provides further evidence that my brother's motive for
talking to the media about me was not what he claimed, to
"humanize" me and decrease my risk of suffering the death
penalty. If that had been his motive he would have taken a
softer approach, comparable to that of his 1981 letters,
which recognized that there were two sides to the story.
Instead, he took a hard line and portrayed me in a way that
was certainly not calculated to win the sympathy of a judge
or a jury.

\*\*\*\*\*\*

I want to reiterate that I believe Ms. Tarmichael to be
under normal circumstances a very decent and kindly person.
Sexual peculiarities are of course commonplace and when she
gave expression to hers in regard to me I'm sure that she
had no idea of how badly she was hurting me - since she
knew nothing about my past history.
I've included this chapter only to put before the public
the truth about a matter that has been badly misrepresented
in the media. I ask journalists to refrain from harassing
Ms. Tarmichael with questions about this affair. It's
doubtful that they will honor this request, but if they
don't it will be further evidence of the irresponsibility
of the majority of media people.

## NOTES TO CHAPTER X

1. (Ba) Journals of TJK, Series VI #2, July 17, 1978, pp.
1-3.

2. Same, July 17, 1978, pp. 2-5.

3. Same, August 26, 1978, p. 43.

4. (Ha) *NY Times Nat.*, May 26, 1996, p. 24, columns 2, 3.

5. (Hb) *Washington Post*, June 16, 1996, p. A21.

6. (Ba) Journals of TJK, Series VI #2, August 23, 1978, pp.
33, 34.

7. My brother fired me on Wednesday, August 23 ((Ba)
Journals of TJK, Series VI #2, August 23, 1978, pp. 32, 33).
As I remember it, I was hired by Prince Castle on Friday,
August 25, and began work there on Monday, August 28.
Whether or not my memory is accurate on this point, it is
certain that I had begun work at Prince Castle by Thursday,
August 31, since on September 1 I wrote in my journal,
"Yesterday I felt extremely bad again. But when I got home
from work in the evening...  ." (Ba) Journals of TJK,
Series VI #4, September 1, 1978, p. 5.

8. (Cb) FL Supplementary Item #11, letter from me to Ellen
Tarmichael, August 25, 1978.

9. (Ba) Journals of TJK, Series VI #2, July 17, 1978, p. 1.

10. Same, July 17, 1978, pp. 1, 2.

11. Same, July 17, 1978, p. 3.

12. Same, July 17, 1978, pp. 3-5.

13. Same, July 17, 1978, pp. 5-10. This journal entry was
written on the day after the events it describes, since we
find on p. 3: "I figured I would just...drop in on her
unannounced on Sunday (yesterday) afternoon."

14. (Ad) Autobiog of TJK 1988, p. 16: "[A]t the age of
36 I found an intelligent and attractive 30-year old woman
(call her Miss T.)... . I'd heard vague rumors to the
effect that there was something funny about her, but beggars
can't be choosers, so I took my chances... ."

15. (Ba) Journals of TJK, Series VI #2, July 29,
1978, p. 10.

16. Same, August 23, 1978, p. 30. I recorded this remark of
Ellen's almost four weeks after the dinner date, and I did
not state in my journal that the remark was made on that
date, but I remember it as having been made at that time. In
any case, it matters little whether Ellen made the remark
then or at some other time.

    In the early months of 1979 I wrote:

    "In 1978 I knew a woman named Ellen Tarmichael. Once she
told me that if anyone ever played a dirty trick on her she
would get revenge no matter what; she would do anything, no
matter how underhanded, etc., etc. She sounded so
unscrupulous that I started to feel a little uneasy with
her. Later that same day, she started giving me a spiel
about how she felt everyone had a duty to help society and
all that kind of stuff. I asked her how she would square
this with the vengeful attitudes she had been expressing
earlier. She said, 'Well, those ideas of revenge are only
things that I fantasize. I have never actually done anything
like that.' " (Ac) Autobiog of TJK 1979, pp. 102, 103.

17. (Ba) Journals of TJK, Series VI #2, July 29,
1978, pp. 10-15.

18. Same, August 23, 1978, p. 21.

19. (Ad) Autobiog of TJK 1988, p. 17 "[S]he refused to
walk alongside me and insisted on walking a couple of feet
behind."

20. (Ba) Journals of TJK, Series VI #2, August 23,
1978, pp. 21,22.

21. Same, August 23, 1978, pp. 22-25.

22. (Ad) Autobiog of TJK 1988, p. 17: "From my own
experience with her, from what I'd heard about her, and
from things that she said, I concluded that she was probably
a sadist who got a sexual kick out of humiliating men."

23. (Ba) Journals of TJK, Series VI #2, August 23,
1978, p. 24.

24. Same, August 23, 1978, pp. 25-27.

25. (Cb) FL Supplementary Item #11, letter from me to Ellen
Tarmichael, August 25, 1978, p. 6.

26. (Ba) Journals of TJK, Series VI #2, August 23, 1978, p.
27, and August 26, 1978, p. 44.

    The media have stated that at work I made "loud, crude
remarks" about Ellen. ((Ja) *Mad Genius*, p. 53; (Jb)
*Unabomber*, pp. 97, 98.) This is false. Apart from
the limericks there was some hostile eye contact between us,
and at one point I pinched her behind, but I made no
offensive remarks to her or about her. I might have done so
later if my brother had not interfered by firing me, but I
did not in fact do so. If I *had* made offensive remarks they
would not have been loud. Everyone who knows me at all well
knows that that just isn't my way. See (Ba) Journals of
TJK, Series VI #2, August 23, 1978, pp. 26-32, where are
described the interactions between Ellen and me from the
time I pasted up the limericks to the time when my brother
fired me.

27. (Ba) Journals of TJK, Series VI #2, August 23,
1978, p. 32.

28. Same, August 23, 1978, pp. 32, 33.

29. Same, August 23, 1978, pp. 33-35.

30. (Ca) FL #458, letter from me to my mother, July 5,
1991, p. 2: "[You'll remember what happened when Ellen
Tarmichael...intentionally and cruelly hurt and
humiliated me, and I retaliated by trying to embarrass her.
*Refusing to listen to my side of the story*, Dave (as well
as you and Dad) jumped down on me and treated me as if I
were some kind of a monster."

31. (Ba) Journals of TJK, Series VI #2, August 23,
1978, pp. 35, 36.

32. Same, August 23, 1978, p. 40, and August 26,
1978, pp. 40-43.

33. (Ja) *Mad Genius*, p. 53.

34. If I wanted to be nasty, I could say that he "shut
himself up in his room for days at a time." He certainly
spent at least as much time in his room as I did in mine.

35. (Ca) FL #458, letter from me to my mother, July 5,
1991, p. 2: "[E]ven after I had fully explained to you
what had happened, not one of you three apologized to me or
said a single word in sympathy for my pain. To do Dave
justice,...*a couple of years later* he did apologize... ."

36. (Ha) *NY Times Nat.*, May 26, 1996, p. 24, column 3.

37. (Ba) Journals of TJK, Series VI #4, September 1, 1978,
pp. 5, 6.

38. (Ca) FL #245, letter from David Kaczynski to me, late
summer or fall of 1981, pp. 2, 3.

39. (Ca) FL #247, letter from David Kaczynski to me, late
summer or fall of 1981, p. 1.

40. The three dots are in the original.

41. (Ca) FL #247, letter from David Kaczynski to me, late
summer or fall of 1981, p. 3.
