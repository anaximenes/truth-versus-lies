# CHAPTER II

My mother, my brother, and the media have portrayed me as
socially isolated to an abnormal degree from earliest
childhood. For example, shortly after my arrest, *Time*
reported: "Investigators were told that in childhood Ted
seemed to avoid human contact." ¹

According to Investigator #1's interview with my mother,

"As he grew older (age 2-4) Wanda spent a great deal of
time attempting to get Ted to play with other kids, mostly
without success. Friends and relatives always told her Ted
was too clingy, so she attempted to encourage his
interaction with other children. She would invite children
from the neighborhood over to play, only to have Ted leave
the group and go to his room to play alone. She said he
always managed to have one friend at a time, but would
rebuff the attempts of friendship from all other children.
Wanda also took Ted to a play school for children for an
hour or so each week so that he could play with other kids.
Ted didn't mind going, but would play alongside the other
children instead of with them. Ted would get angry if
another child tried to join or interfered with what he was
doing. Ted went to preschool and kindergarten, and seemed to
enjoy it. The teachers did not complain about his behavior,
but did mention Ted always wanted to work on projects alone,
and did not interact with other children." ²

The *Washington Post* told a similar tale on the basis of an
interview with my mother. ³

Here again the documentary evidence shows that my mother is
lying. I will not try the reader's patience by addressing
all of her false statements, but will stick to the essential
point, that my interaction with other children was normal
until, at about the age of 11, I began to have serious
social problems for reasons that will be made clear later.

According to the pediatricians who examined me:

"April 4, 1945... Plays well with other children. ..."

"May 18, 1950... Healthy boy. Well adjusted. ..."

"May 8, 1951... Plays well with children in school and
neighborhood. Very happy." ⁴

The doctors could have obtained this information about my
social adjustment only from my mother. It was always she,
and not my father, who took me to my examinations at the
University of Chicago clinics.

Thus, statements of my mother's that were recorded during
my childhood clearly contradict her recent statements
concerning my early social development. If she wasn't lying
then, she is lying now. Either way, the record shows her to
be a liar.

What then is the truth concerning my social adjustment in
early childhood? My mother's reports to doctors carry
little weight because, as we will show later, she often did
lie in order to present a favorable picture of me to persons
outside the immediate family. But since the Baby Book was
private there is no particular reason to doubt the
statements she made there that show that I was not socially
withdrawn.

It's true that at one point in the Baby Book my mother
indicated I was somewhat shy, ⁵ as noted in Chapter I, and
I myself have a vague memory of being a little shy up to the
age of five or so. Furthermore, I wrote in my 1959
autobiography:

"As far as I can remember, I have always been socially
reserved, and used to be rather unpleasantly conscious of
the fact. For example, I remember that when I was very
little, 3 or 4 years old, I was very concerned over the fact
that when my mother bought me an ice cream cone, I was
always afraid to take it directly from the lady's hand; my
mother had to take it from her and give it to me. Eventually
I overcame this. ...

"I learned to whistle and to swim later than most of my
companions,\[text unknown\] did learn to skate. And it
often bothered me that I was less socially active than the
rest of the boys, which I think was partly due to shyness
and partly due to a certain lack of interest in some of
their activities. I've always kept to myself a lot." ⁶

The second paragraph of this passage evidently applies not
to my earliest years but to a much later period when I did
indeed have social problems. As a result of these problems I
began to take a perverse pride in being unsocial, and this
is probably what led me to imply (as I did in the first
paragraph above) that I was "socially reserved" even in my
earliest years.

But even if that first paragraph is taken at face value,
there is plenty of evidence to show that my social
interaction with other children was easily within the normal
range until my real problems began in early adolescence. As
we saw in Chapter I, my mother indicated in the Baby Book
that at the age of one year I was consistently friendly to
other children:

"*Is he usually shy or friendly with strange women*? either
*men*?  either *children*? friendly... ." ⁵

From age one to three I developed a close friendship with
Adam Ks., a boy about eight months older than I was. The
attachment left a long-lasting impression on both of us. He
was the son of the couple who occupied the first floor of
the house of which my parents and I had the second story;
when we moved to another house I was separated from him. ⁷

In the new house we again occupied the second story, and
with the little girl downstairs, Barbara P., I formed
another strong attachment, ⁸ though it was not as strong
as my attachment to Adam. During this same period (age 3 to
4) I had at least one other frequent playmate, whose name,
if I remember correctly, was Jackie. ⁹

Shortly before my fifth birthday we moved to a house on
Carpenter Street (the first house that my parents owned),
¹⁰ and from that time until I entered Harvard I always had
several friends. My friends on Carpenter Street included
Johnny Kr., Bobby Th., Freddie Do., Jimmy Bu., Larry La.,
and Mary Kay Fy. ¹¹ As long as we lived on Carpenter
Street, I attended Sherman School, a unit of the Chicago
public-school system. All of my friends on Carpenter Street
either attended the Catholic school or were a year older
than I was, so that they were in a different grade.
Consequently my school friends were not the same as those
with whom I played near home. My school friends included
Frank Ho., Terry La C., Rosario (an Italian kid whose last
name I do not remember) and Peter Ma. ¹²

I not only had friends but, on a few occasions, exercised
leadership. For example, I once came up with the idea of
putting on a "carnival," as we called it. I persuaded
Johnny Kr. and Bobby Th. to help me arrange games and simple
entertainments, and after advertising the event by word of
mouth for several days we made up tickets by hand, sold them
to neighborhood kids, and made a modest profit. ¹³

Thus there is no truth in my mother's portrayal of me as
abnormally solitary from early childhood. There was no need
for her to "invite children from the neighborhood over to
play," ¹⁴ nor did she ever do so during these years as
far as I can remember.

The first indication of any significant social difficulties
on my part came when I was perhaps eight or nine years old,
¹⁵ and it very likely resulted from the fact that our
family was different from its neighbors. My father worked
with his hands all his life; my mother, apart from teaching
high school English for two years during her fifties, never
did anything more demanding than lower-level secretarial
work; and our family always lived among working-class and
lower-middle class people. Yet my parents always regarded
themselves as a cut above their neighbors. They had
intellectual pretensions, and though their own intellectual
attainments were extremely modest, to say the least, they -
especially my mother - looked down on their neighbors as
"ignorant." (But they were usually careful not to reveal
their snobbish attitudes outside the family.) ¹⁶

Our block of Carpenter Street was part of a working-class
neighborhood that was just one step above the slums. As my
playmates grew older, some of them began engaging in behavior
that approached or crossed the line dividing acceptable
childhood mischief from delinquency. ¹⁷ For example, two of
them got into trouble for trying to set fire to someone's
garage. ¹⁵ I had been trained to a much more exacting
standard of behavior and wouldn't participate in the other
kids' mischief. ¹⁸ Once, for instance, I was with a bunch
of neighborhood kids who waited in ambush for an old
rag-picker, pelted him with garbage when he came past, and
then ran away. I stood back in the rear and refused to
participate, and immediately afterward I went home and told
my mother what had happened, because I was shocked at such
disrespect being shown to an adult - even if he was only a
rag-picker. ¹⁹

So it may be that the reason why I ceased to be fully
accepted by my Carpenter-Street playmates at around the age
of eight or nine was that they saw me as too much of a
"good boy." In any case they did seem to lose interest in
my companionship - I was no longer one of the bunch. ²⁰ I
continued to get along well with the kids in school. ²¹
Unlike the kids on my block they showed no tendency to
serious mischief, either because they were better-behaved
kids or because the supervised environment of school left
few opportunities for misbehavior.

My parents noticed the fact that I was becoming isolated
from my Carpenter-Street friends, and they repeatedly
expressed to me their concern that there might be something
wrong with me because I was not social enough. ¹⁵ To me it
was acutely humiliating to be pushed out to the fringe by
these kids with whom I had formerly associated on an equal
basis, and I was too ashamed to tell my parents what was
really happening, or even to admit it to myself until many
years later. My mother invented an explanation for my
isolation that was consistent with her intellectual
pretensions: I wasn't playing with the other kids because I
was so much smarter than they were that they bored me. This
was absurd. I was bored with the other kids when (as often
happened) they moped around aimlessly rather than pursuing
some activity, but there can be no doubt that I wanted to
continue playing with them and was deeply hurt by the fact
that I was no longer fully accepted. Yet, because my
mother's explanation soothed my vanity, I half-believed it
myself. In a very brief (one and a quarter-page)
autobiographical sketch that I wrote at the age of fifteen,
I said:

"Beginning in the second or third grade I began to become
somewhat unsocial, keeping to myself and seeking the
companionship of my comrades less often. This was probably
due, in part, to the level of education and culture in my
old neighborhood, where no one was interested in science,
art, or books." ²²

Actually, I wasn't so terribly interested in science, art,
or books myself. The autobiographical sketch was part of an
application for admission to Harvard and therefore was
written under the close supervision of my mother. Rereading
it now I feel almost certain that the first paragraph of it
was actually composed by her. That paragraph is written in a
kind of language that I rarely use now and that I can hardly
imagine myself having used at the age of fifteen; but it's
just the sort of thing that my mother would write. ²³

I'm quite sure that my partial isolation from the
Carpenter-Street kids did not begin before I was eight, at
the earliest, and that I had no serious problems with the
kids in school at the time. Yet the sketch refers to "the
second or third grade," which would make me seven or eight
years old. Possibly my mother's hand is seen here too.

Notwithstanding all of the foregoing, I think my parents had
an inkling of the fact that the bad behavior of the other
kids had something to do with my isolation. Not long after
my tenth birthday we moved to Evergreen Park, a suburb of
Chicago, and my mother told me many years later that she and
my father had decided to move mainly so that I "would have
some decent kids to play with." Though my mother is hardly
a reliable source of information, her statement is probably
true in part; yet it's likely that there were also other
reasons for the move. Not far from where we lived, a case
of "block-busting" ²⁴ gave rise to some very serious
race-riots that were essentially territorial conflicts
between the black and the white working class. All white
householders in the area were put under pressure to place in
their windows a small sign saying, "This property is not
for sale," which was intended as a show of white solidarity
against black "intrusion." My parents had very liberal
attitudes about race and felt that it was against their
principles to put up such a sign. But they received a
threat, and, fearing that I might be attacked on my way to
school, they gave in and placed the sign. ²⁵ This was
extremely upsetting to them and it must have contributed to
their decision to move out to the suburbs.

\* \* \* \* \*

Meanwhile, when I was a bit less than seven-and-a-half years
old, I had acquired a baby brother. My brother David for
many years has argued that I unconsciously hate him because
the attention that my parents devoted to him on his arrival
made me feel deprived of their affection. ²⁶

The *New York Times* quoted my aunt Josephine Manney, née
Kaczynski, as follows:

" 'Before David was born, Teddy was different,' the aunt
said. 'When they'd visit he'd snuggle up to me. Then, when
David was born, something must have happened. He changed
immediately. Maybe we paid too much attention to the new
baby.' " ²⁷

Little did my aunt Josephine know the *real* reason why I
stopped snuggling up to her! I'll explain in a moment. But
first let me make it clear that I'd never heard anything of
this sort from Josephine before I read the *New York Times*
article, and it's evident that my brother never heard it
either, since, in our discussions of his theory about my
reaction to his birth, he never mentioned any such statement
on the part of our aunt; nor did he ever cite any other
rational evidence in support of his theory. The theory,
apparently, grew entirely out of his own imagination.

As to the real reason why I stopped snuggling up to my aunt:
Josephine was a good-looking woman; though she was over
forty at the time of my brother's birth, she'd kept herself
in shape and was still attractive. I don't know whether it
was normal or precocious, but by the age of about seven I
already had a fairly strong interest in the female body. ²⁸
Not long after my brother's birth, my family and I visited
the apartment where Josephine lived with her mother (my
paternal grandmother). My aunt and I were sitting on a
couch, and, attracted by her breasts, I slid over against
her, put my arm over her shoulder, and said, "Let's play
girlfriend." Josephine laughed and put her arm around me,
and I had the decided satisfaction of feeling her breast
against my body. My aunt just thought it was cute, but my
mother was sharp enough to see what was really going on.
After a short interval she said, "I think I'll go to the
store and get some ice cream" (or maybe it was candy or
something else), and she invited me to come with her. I
declined, but she insisted that I should come. As soon as
she got me out of the house she gave me a tongue-lashing and
a lecture on appropriate behavior with ladies. It will not
surprise the reader that, from then on, I kept my distance
from Josephine.

To return to my brother's theory that I resented his
arrival in the family: He first indicated his suspicion that
I unconsciously hated him in a letter to me written some
time during the summer of 1982. That letter has not been
preserved, but there is a reference to it in a letter that I
sent to my brother in 1986. I wrote: "I recall that a few
years ago you said you had feared that I had (as you put it)
a hatred for you so great that even I was unable to
acknowledge it." ²⁹

In a letter that he wrote to me in 1986, my brother
expounded his theory as follows:

"You should have hated me, in that as a new baby in the
family, the new locus of affection, I should have awakened
your fears of abandonment. \[My brother is referring here
to the alleged "fear of abandonment" that I was supposed
to have as a result of "that hospital experience."\] The
parents tell me that just the opposite was true, that you
were extremely affectionate toward me and that you didn't
show any jealousy whatsoever. I have thought of a way to fit
this in, by recourse to the Freudian theory of 'Denial.'
When you saw the murdered babies in the Nazi camp, it might
have awakened your horror as a secret wish fulfillment in
respect to me. \[My brother is referring here to a dream
that I once had about him, concerning which I will have more
to say shortly.\] When you vowed to protect me at the
expense of your own life, perhaps the one you vowed to
protect me from was *yourself*, I have no idea how much or
little truth there may be in this interpretation." ³⁰

The disclaimer in the last sentence is perhaps disingenuous,
as my brother has clung to the theory persistently over the
years. According to the *New York Times*, "David said his
mother told him that she gradually encouraged Ted to hold
him and that 'from that time forward, he showed a great
deal of gentleness toward me.' " ³¹ The implication,
that I had resented him at first, is contradicted by my
brother's own statement, quoted above, that "\[t\]he
parents tell me that... you were extremely affectionate
toward me and that you didn't show any jealousy
whatsoever." It is also contradicted by a statement of my
mother's: "Ted seemed to easily accept having a brother in
the house, and liked to hold David when he was a baby." ²

As I remember it, prior to my brother's birth my parents
told me repeatedly that the new baby, when it came, would
require a great deal of care and attention, and that I must
not feel that my parents loved me any less because they were
devoting so much time to the baby. When David was born I
wondered why my parents had put so much emphasis on this
point, because I by no means felt left out or deprived of
attention. As I wrote in my 1979 autobiography:

"My brother David was born when I was 7½. I considered this
a pleasant event. I was interested in the baby and enjoyed
being allowed to hold it. ...

"One reads much about 'sibling rivalry' - the older child
supposedly resents the new baby because he feels it has
robbed him of his parents' affection. I do not recall ever
having had any such feeling about my baby brother. ... I
think my parents were aware of the problem of 'sibling
rivalry' and made a conscious effort to avoid this problem
when the new baby came ." ³²

In those years my parents and I got all our medical care at
the University of Chicago teaching hospitals, which were
among the finest in America, and the doctors no doubt had
talked to my parents about the way to handle my relationship
with my new brother.

Why then does my brother think that I have an intense,
unconscious hatred for him? People often attribute their own
motives and impulses (including unconscious ones) to other
people. Further on in this book we will show that my brother
has a hatred for me that he has not acknowledged - probably
not even to himself. At the same time he has a strong
affection for me, and it appears that he has never faced up
to the profound conflict between his love and his hatred. My
brother habitually retreats from conflicts rather than
struggling with them.

My feelings toward my brother in his infancy are well
illustrated by a dream that I described to him in a letter
that I sent him during the summer of 1982. After making some
highly critical comments about his character, I wrote:

"I am going to open to you the window to my soul as I would
not open it to anyone else, by telling you two dreams that
I've had about you. The first dream is simple. It is one I
had more than thirty years ago, when I was maybe 7 or 8
years old and you were still a baby in your crib. Some time
before, I had seen pictures of starving children in Europe
taken shortly after world war II - they were emaciated, with
arms like sticks, ribs protruding, and guts hanging out.
Well, I dreamed that there was a war in America and I saw
you as one of these children, emaciated and starving. It
affected me strongly and when I woke up I made up my mind
that if there was ever a war in America I would do
everything I possibly could to protect you. This illustrates
the semi-maternal tenderness that I've often felt for you."
³³

In reply to the foregoing letter my brother wrote to me
expressing his gratitude for the affection I had expressed,
and for the fact that I "cared for \[him\] more than anyone
else ever had." He then added the remark mentioned
earlier - that until then he had feared that I had a
hatred for him so great that I could not acknowledge it. ³⁴

I referred to this letter of my brother's in a note that I
wrote him in September, 1982:

"I received your last letter and note that it shows your
usual generosity of character. Instead of being sore over
the negative parts of my attitude toward you, you were
favorably impressed by the positive parts." ³⁵

My brother does have a good deal of generosity in his
character, but I now think that the nature of his reaction
to my letter was less a result of generosity than of his
tendency to retreat from conflict.

\* \* \* \* \*

Not long after my brother's birth my mother's personality
began to change. The cause may have been post-partum
depression, a hormonal imbalance brought about by her
pregnancy, or something else, but, whatever the reason, she
began to grow increasingly irritable. ³⁶ The symptoms were
relatively mild at first, but they worsened over the next
several years so that, by the time I reached my teens, she
was having frequent outbursts of rage that express
themselves as unrestrained verbal aggression, sometimes
accompanied by minor physical aggression ³⁷ (though never
enough of the latter to do any physical harm).

The change in my mother's personality affected my father
and brought about a gradual deterioration of the family
atmosphere. I described this in a 1986 letter to my brother:

"You don't realize that the atmosphere in our home was
quite different during the first few years of my life than
it was later. You know how it was during my teens - people
always squabbling, mother crabby and irritable, Dad morosely
passive. Too much ice cream, candy, and treats, parents fat
and self-indulgent. A generally *low-morale* atmosphere. But
it was very different up to the time when I was, say, 8 or 9
years old. Until then, the home atmosphere was cheerful,
there was hardly any quarrelling, and there was a generally
*high-morale* atmosphere. Ice cream and candy were
relatively infrequent treats and were consumed in moderation
... . Our parents were more alive and energetic. When
punishment was necessary it was given with little or no
anger and was used as a more-or-less rational means of
training; whereas during my teens, when I was punished it
was commonly an expression of anger or irritation on the
part of our parents. Consequently this punishment was
*humiliating*. The more-or-less rational punishment of the
early years was not humiliating." ³⁸

## NOTES TO CHAPTER II

1. (Hg) *Time*, April 22, 1996, p. 46.

2. (Ka) Interview of Wanda by Investigator #1, p. 2.

3. (Hb) *Washington Post*, June 16, 1996.

4. (Ea) Med Records of TJK, U. Chi.; April 4, 1945, p. 26;
May 18, 1950, p. 51; May 8, 1951, p. 51.

5. (Bc) Baby Book, p. 122.

6. (Ab) Autobiog of TJK 1959, p. 2.

7. (Bc) Baby Book, pp. 113, 115; (Ac) Autobiog of TJK 1979,
pp. 1, 2.  In (Qb) Written Investigator Report #68, Adam Ks.
himself confirms the strength of this friendship. However,
much of the information he gives is incorrect.

8. (Ac) Autobiog of TJK 1979, p. 3.

9. Jackie was the four-year-old boy referred to on p. 1
of (Ac) Autobiog of TJK 1979.

10. (Ab) Autobiog of TJK 1959, p. 2; (Ac) Autobiog of TJK
1979, p. 5; (Ga) Deed #1.

11. (Ac) Autobiog of TJK 1979, pp. 5, 6, 10, 11, mentions
all these friends by name.

12. (Ac) Autobiog of TJK 1979, pp. 6-8 describes my
relations with Frank Ho., Terry La C., and Rosario. My
friendship with Peter Ma. is not documented.

13. (Ac) Autobiog of TJK 1979, pp. 10, 11.

14. (Ka) Interview of Wanda by Investigator #1, p. 2.

15. (Ac) Autobiog of TJK 1979, p. 12.

16. (Ac) Autobiog of TJK 1979, pp. 17, 24, 79; (Na) FBI 302
number 2, p. 6.

17. (Ac) Autobiog of TJK 1979, pp. 12, 194.

18. (Ab) Autobiog of TJK 1959, p. 3; (Ac) Autobiog of TJK
1979, pp. 12-14, 16, 17, 194; (Ca) FL#458, letter from me
to my mother, July 5, 1991, pp. 9, 10.

19. (Ac) Autobiog of TJK 1979, p. 194; (Ca) FL#458, letter
from me to my mother, July 5, 1991, pp. 9, 10.
"Rag-pickers" were very poor people who made their living,
such as it was, by picking through trash to find anything
that could be sold as scrap.

20. (Ac) Autobiog of TJK 1979, p. 12; (Ca) FL#458, letter
from me to my mother, July 5, 1991, p. 9.

21. (Ac) Autobiog of TJK 1979, p. 12; (Ca) FL#458, letter
from me to my mother, July 5, 1991, p. 10.

22. (Aa) Autobiog of TJK 1958. When, in (Ab) Autobiog of TJK
1959, p. 2, I wrote, "I was less socially active than the
rest of the boys,... partly due to shyness and partly
due to a certain lack of interest in their activities," I
probably was still under the influence of my mother's
theory that I was bored with other kids because I was
smarter.

23. The first paragraph of this document ((Aa) Autobiog of
TJK 1958) reads:

    "My first vague memories are of a golden age of blessed
irresponsibility. But the grass is always greener on the
other side of the fence, and I suppose at that time I looked
forward to the unbounded joys of growing up."

24. "Block-busting" was a practice whereby unscrupulous
realtors would contrive to sell to black people a house on a
white-occupied block near black territory. White
householders on the block, fearing that they would be left
isolated in the midst of a black neighborhood, sold off
their property as quickly as possible. Thus the realtors
were able to buy houses from whites at reduced prices and
sell them again to black families at inflated prices.

25. This account of the placement of the sign is based in
part on what I myself observed at the time, but also in part
on what my mother told me many years later. Given my
mother's unreliability, it cannot be assumed that the
account is strictly accurate.

26. (Ha) *NY Times Nat*., May 26, 1996, p. 22, column 3;
(Ca) FL #330, letter from David Kaczynski to me, March or
April, 1986, p. 14; (Ca) FL#331, letter from me to David
Kaczynski, April 16, 1986, pp. 3, 4.

27. (Ha) *NY Times Nat*., May 26, 1996, p. 22, column 3. The
*Times* quoted only an "aunt" who preferred to remain
anonymous, but the aunt in question is obviously Josephine.
I have just four living aunts: Sylvia, Madeline (aunts by
marriage), Freda, and Josephine. Sylvia married my uncle
Benny when I was in my teens, and I'd never met her before
that time; I was never chummy enough with Madeline to
"snuggle up" to her; and Freda informed me in (Cb) FL
Supplementary item #6, letter from Freda Tuominen to me,
July 20, 1996, that she was not the unnamed aunt quoted by
the *Times* (which I already knew from the content of the
quotations). So that leaves Josephine.

28. (Ac) Autobiog of TJK 1979, pp. 11, 20.

29. (Ca) FL #331, letter from me to David Kaczynski, April
16, 1986, p. 4.

30. (Ca) FL #330, letter from David Kaczynski to the author,
March or April, 1986, p. 14.

31. (Ha) *NY Times Nat.*, May 26, 1996, p. 22, column 3. In
this same column we find:

    "David said his parents told him about how his father,
grandmother and Teddy had gone to the hospital after his
birth. ... 'So my father and grandmother left Ted in the
lobby and went up to visit me,' he said, 'When they all
went down to the lobby... he was sitting there alone in
tears and very deeply upset.'" I don't remember any such
incident, and I doubt that it happened. My brother is very
prone to get his facts garbled.

32. (Ac) Autobiog of TJK 1979, pp. 17,18.

33. (Ca) FL #266, letter from me to David Kaczynski, Summer,
1982, pp. 5, 6. I described the dream in nearly identical
terms in (Ac) Autobiog of TJK 1979, pp. 17, 18, and added
that "I felt a sense of pity and love toward my
brother... ."

    Characteristically, my brother got the dream garbled in
the 1986 letter of his that we quoted a few pages back:
"When you saw the murdered babies in the Nazi camp... When
you vowed to protect me at the expense of your own
life... ." (See Note 30 above.) Compare this with the
correct account of the dream. Later we will see other
instances in which my brother has gotten his facts garbled.

34. This letter has not been preserved, and I am
relying here on memory and on the 1986 letter in which I
mentioned the remark about "great hatred." See Note 29
above.

35. (Ca) FL #271, letter from me to David Kaczynski,
September, 1982, p. 2.

36. (Ca) FL #458, letter from me to my mother, July 5, 1991,
p. 9. (Ca) FL #423, letter from me to my mother, January 15,
1991, pp. 4, 5: "I always felt you were a good mother to me
during my early years. It was when I was around 8 years old
that your behavior and the family atmosphere began to
deteriorate, and it was during my teens that I was subjected
to constant, cutting insults such as imputations of
immaturity or mental illness." My Xerox copy of the copy of
this letter that I mailed to my mother is illegible in
places. Therefore, for one line of the foregoing quotation I
had to refer to p. 2 of the copy of this letter that I kept
in my cabin.

37. Example of minor physical aggression is given in (Ac)
Autobiog of TJK 1979, p. 47 (throwing saucepan).

38. (Ca) FL #339, letter from me to David Kaczynski, May,
1986, pp. 3, 4. A similar account is given in (Ac) Autobiog
of TJK 1979, pp. 38, 39. For confirmation see (Ca) FL#458,
letter from me to my mother, July 5, 1991, p. 9. (Ab)
Autobiog of TJK 1959, p. 5, has: "My relationship with my
parents used to be generally affectionate, but the last few
years it has deteriorated considerably... ."
