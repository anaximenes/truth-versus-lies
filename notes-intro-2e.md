## NOTES TO INTRODUCTION

1.  (Hp) Daily Oklahoman, June 12, 1995.

2.  Envelope X; see the three sheets marked with a green
letter A at the top.

3. I am considering here only (Qb) Written Investigator
Reports. I am leaving out of consideration (Kb) Lincoln
Interviews, of which I have made very little use in this
book, and which I have not taken the trouble to tabulate;
except to the extent that some of the Lincoln interviews
also occur among the Written Investigator Reports.

   I am considering here only the Written Investigator
Reports that I have received as of March 6, 1998. If I
receive more such reports later, I will not bother to
change the tabulation.

4. To experimental psychologists, "long-term" memory
means any memory spanning more than thirty seconds. But here
I use the expression "long-term" to indicate memories of
events that have occurred years or decades in the past.

    I have often been surprised to find that other people
have failed to remember things that I remember quite
clearly. Here is an example:

    When I took German R at Harvard I sat next to a student
named Kostinski. We had similar last names and we were the
two best students in the class; he was best and I was
second-best. Nine or ten years later when I was at Berkeley,
in a building that contained the offices of some of the math
department's junior faculty and graduate students, I
encountered Kostinski, who was pacing back and forth
absorbed in thought. I accosted him, saying, "Weren't you
in German R at Harvard?" He looked at me blankly. "German
R...?" To prod his memory I mentioned the instructor's
name. "Miss Dreimanis." Kostinski broke into a broad smile
and exclaimed, "Oh! Were you in that class?" I chatted
with him for a few minutes, and he told me that he was a
graduate student in the math department and was working on
his doctoral dissertation. "I thought you were pre-med," I
said. He answered, "I was, but I switched to math." Thus I
correctly recalled Kostinski's name, his face, and the
career he'd planned at the time I knew him, while he did
not remember me at all, nor did he remember the designation
of Miss Dreimanis's course (German R).

    I am relying on memory for this thirty-year-old
anecdote, but any reader who is sufficiently interested can
check it out. It shouldn't be very difficult to determine
whether the Berkeley math department in 1967, 1968, or 1969
had a graduate student named Kostinski who had taken German
R at Harvard in the fall of 1958 and got an A in it.

5. Investigators #2 and #6 told me this at least three
times during 1996 and early 1997. In the fall of 1997 I
asked for confirmation and received it orally (Qa). Oral
Report From Investigator #2, November 10, 1997 reads: "My
long-term memory is unusually accurate - confirmed by
\[Investigator #2\] and \[his/her\] investigators who
asked Investigator #2 for written confirmation and he/she
gave me the following:

   "Ted appears to have a good long term memory. Many
people who have been interviewed have concurred with Ted's
recollection of certain events. For example, Ted recalled
that in college he had a classmate X\_\_\_\_Y\_\_\_\_, who
rocked back and forth and Prof. Y\_\_\_\_ confirmed this
account. \[Actually I remembered only the first name of this
classmate; I'm not sure I ever knew his last name.\] Ted
has been able to recall names of teachers and people he knew
from over thirty years ago as well as addresses, dates of
birth and literature from childhood. \[I don't know what
dates of birth or literature Investigator #2 is referring
to.\] He has also recalled floor plans of residences and
accurate maps of campuses that he hasn't been at in over
thirty years \['accurate maps of campuses' should be
deleted\]." (Qc) Written Reports by Investigator #2, p.2.

    I pointed out to Investigator #2 that "Ted appears to
have a good long term memory" was a considerably weaker
statement than the ones he/she had earlier given me orally.
Investigator #2 agreed, said that the earlier, stronger
statements were correct, and changed his/her written report
to read: "Ted has a remarkably good long term memory. ..."
(Qc) Written Reports by Investigator #2, p.2.

6. (Ca) FL#423, letter from me to my mother, January 15,
1991, pp. 6,7: "What I especially hope you haven't thrown
out is some old letters of mine. ... I'm interested in
the accuracy of long-term memory. ...  So I'd appreciate
it if you could send me either the letters, or photocopies
of them... . If it is not convenient for you to crawl up in
the attic to rummage around for the letters, then of course
you need not do so." (Ca) FL#424, letter from my mother to
me, late January, 1991: "I'm too short and stiff to be able
to climb safely into the attic... . *However*, I did find
a box full of letters from you in your foot locker. ...
I'll send you the box full... ."


   My mother did send me these letters, which comprised
almost all of the letters from me that she'd saved from
about 1968 through the 1980's, but I never even got around
to glancing at them before my arrest. Later, when I was in
jail, I was given copies of these letters as well as the
older letters (1957-1968) that had been stored in the attic,
and other letters written by or to members of my family.

   It is because the past is important to me that I have been
interested in the accuracy of long-term memory.

7. (Fc) School Records of TJK, Harvard, p.81.

8. Same, pp. 37, 38.

9. "Ren" is meant as an abbreviation for "Renaissance
thought and literature."

10. "hum gen" is an abbreviation for "human genetics."

11. "Eng intel hist" is an abbreviation for "English
intellectual history."

12. I can think of two exceptions. For one thing, I
remembered incorrectly where my mother got her bachelor's
degree. For another thing, my investigators mentioned to me
that someone had talked about my carrying a briefcase in
high school. I answered that I had carried a briefcase in
eighth grade, but not in high school. The investigators then
pointed out that in 1979 I still remembered carrying a
briefcase in high school, since I recorded in my
autobiography an incident involving a briefcase.
Autobiog of TJK 1979, p. 28. Since I clearly remembered the
briefcase in 1979, I agreed that they were right.
Thinking the matter over later, I thought I remembered it
as a result of having been needled for carrying a briefcase
in eighth grade I had decided not to use one in
high school, and did not use one in my freshman and my
sophomore years, but went back to carrying a
briefcase in my third and last year of high school. Since I
recalled that the briefcase incident had happened in
American History class, I concluded that I must have had
that class in my last-year of high school. I then checked my
high school record and found that this was correct. (Fb)
School Records of TJK, E.P. High School.

13. I remembered the name of Joel S.'s sister as Gloria,
but Joel S. told my investigators that her sister's name
was Diane. (Qb) Written Investigator Report #124, Joel S.,
p.2.

    More significantly, when I wrote my autobiographical
notes in 1979, I remembered that my mother had given my
address to the daughter of a couple who were friends of
my parents because she thought that the young lady and I
had common interests and she hoped we would get together.
This would have made no sense unless the young lady was
living in or near Ann Arbor, where I was at the time; but
she told my investigators that she had never lived in Ann
Arbor. So it seems that my memory of what my mother wrote
me was wrong. (Unless it was my mother who got the facts
garbled, which is possible.) See (Ac) Autobiog of TJK
1979, p.  150.

14. For an example see (Ad) Autobiog of TJK 1988 (corrected
version), pp. 13, 14.

15. (Qc) Written Reports by Investigator #2, p.5.

16. For example, in (Qb) Written Investigator Reports #34,
47, 59, 60, 82, 85, 124, 146, 154, 161, among others.

17. (Qb) Written Investigator Report #154, Leroy
Weinberg, p.2.

18. When I was a teenager, my mother told me that old Mrs.
Butcher, who lived next door to the V.'s, had said to her
that I was *such* a nice boy, because I always returned her
greeting when I passed her, whereas Norma Jean V. often
failed to return her greeting and walked on by without
looking at her.

19. (Qb) Written Investigator Report, #47, Dr. L.Hz.

20. (Kb) Lincoln Interviews, p. 18. I remember a good deal
of what I talked about with R.Cb. and Dr. L.Hz. On one
occasion the patient who preceded me left in a bad mood,
and, because R.Cb. had a suspicion that this man might be a
wife-beater, she phoned his wife and warned her that her
husband was coming home upset. That got us onto the subject
of domestic abuse. I mentioned that some studies had found
that there was about as much physical abuse of husbands by
wives as vice versa. Dr. L.Hz. answered that the wives
probably didn't do much damage because they weren't strong
enough. "I've had women pound on me," he said, "and it
didn't bother me." I replied, "Some women are strong
enough to hit hard." R.Cb. agreed, and mentioned a local
woman who had knocked some man down. I said that some time
earlier I had read an article in a news magazine (probably
*Time*) about domestic abuse. I mentioned that the article
had taken the same position as Dr. L.Hz.: Because women were
smaller they probably didn't do much damage. But, I
continued, in the next issue of the same magazine there was
a letter from an emergency-room doctor who said that in his
experience women often did plenty of damage, because they
were more likely than men to use weapons; he mentioned
husbands who had been slashed with an axe or scalded with
boiling water. As the conversation continued I asked R.Cb.
and Dr. L.Hz., "Why do they \[the abused women\] marry
jerks like that?" R.Cb. and Dr. L.Hz. answered, "Low
self-esteem; maybe their fathers abused them and they think
that's a normal relationship...\[etc.\]." Either R.Cb.
or Dr. L.Hz. mentioned something about a television program
on the subject.

    On other occasions Dr. LHz. and I talked about the
soluble compounds of gold, about gypsum, plaster of Paris,
and Portland cement, and other subjects, and I could go on
and on recounting the details of these conversations, but I
think I've said enough to show that Dr. L.Hz's claim that
I was so quiet as to seem odd is ludicrous.

21. (Qb) Written Investigator Report #34, Dale
Eickelman, pp. 4,5. It is my sophomore year in college, not
high school, that is referred to, since Professor Eickelman
correctly states that I visited his home during the summer
following my freshman year at Harvard.

22. The eight are Larry S., Bob C., Barbara B., Jerry U.,
Bob Pe., Tom Kn., G.Da., Terry L. Six of these eight
friendships are documented, but four are documented only by
my own autobiographies. Two have been confirmed
independently (Bob Pe. by Bob Pe. himself, Tom Kn. by
Tom Kn.'s mother). For references see Chapter III, pp. 79,
87, 88, and associated footnotes. Of the other friends, my
investigators spoke only to one: G.Da., who neither
confirmed nor denied that I was good friends with him. (Qb)
Written Investigator Report #28, G.Da. Actually I was close
to G.Da. only during one school year. With Jerry U. I was
friends from seventh or eighth grade through the summer
following my first year at Harvard; with the others I was
friends for shorter periods. Jerry U., Bob Pe., and Tom Kn.
visited my home, and I visited their homes, on multiple
occasions. I visited the homes of Bob C., G.Da., and Terry L.
on various occasions, but I don't clearly remember that any
of them visited my home. I took two extended excursions with
Bob C. In a letter written in 1958, my mother confirmed that
I had several friends: (Fc) School Records of TJK,
Harvard, p. 18.

23. One reason why Eickelman never encountered any friends
at my house and why I never brought any friends to his house
was that I never much liked him. In fact, I thought
he was somewhat of a creep: (Ac) Autobiog of TJK \[text
unknown\] intended to spend time with him only when he
thrust himself on me \[text unknown\] think of nothing
better to do. Thus, if I had had a friend with me, and if
Eickelman had phoned to suggest that we get together, I
probably would have put him off with some excuse.
(Since our homes were so far apart, Eickelman and I
generally phoned before visiting one another.)

    In his interview with my investigators, (Qb) Written
Investigator Report #34, p.2, Professor Eickelman related a
particularly grotesque anecdote about me. Since he may have
related the same anecdote to the FBI, and since the Justice
Department has a habit of leaking things about my case, I
had better take this opportunity to state that the anecdote
is false. Anyone who knows my mother at all well knows that
I would never have dared to do such a thing in her presence.
If I had done it she would have been horrified beyond all
description; when we got home I would have received a
vicious tongue-lashing and I wouldn't have heard the end of
it for months afterward.

    Professor Eickelman's memory is playing some trick on
him here. He is perhaps recalling something that either he
or I did not in my mother's presence but under very private
circumstances. I could give a plausible explanation for this
recollection of Professor Eickelman's, but I will refrain
from doing so because I am not anxious to reveal information
that would cause embarrassment both to me and to Professor
Eickelman.

24. (Qb) Written Investigator Report #157, G. and D.W., p.4.

25. (Ba) Journals of TJK, Series III #5, March 26,
1975, pp. 32-36.

26. (Ca) FL #154, letter from me to my parents, late March,
1975, pp. 2,3. Both in this letter and in the journal entry
it is mentioned that Pinkston talked to me about the KGB in
a low tone, so that G.Wi. couldn't hear. However, as we were
driving back down off the mountain I told G.Wi. about what
Pinkston had said to me. Moreover, the next spring (1975),
G.Wi. met Pinkston up on the mountain again, and later told
me that Pinkston was a nice, helpful fellow, "but he did
talk a little bit about the KGB." It was on this second
meeting that G.Wi. learned Pinkston's name. Some time later
he told me that Pinkston had died. I understand that Larry
Davis, the local game warden for the Lincoln area at the
time, had been bringing groceries up to Pinkston, and it's
possible he may be able to confirm some of this information.

27. (Qb) Written Investigator Report #87, Russell Mosny
1996, p.1.

28. (Ac) Autobiog of TJK 1979, p. 25.

29. (Qb) Written Investigator Report #30, L.D., p.2.

30. (Ac) Autobiog of TJK 1979, p.21.

31. (Da) Ralph Meister's Declaration, p. 2, paragraph 7.

32. Same, pp. 2,3, paragraphs 8-10.

33. For example, (Qb) Written Investigator Reports #6, K.B.,
p.1; #134, Lois Skillen, p.8; #152, E. Wr., p.3. Also see
Note 57.

34. (Qb) Written Investigator Report #151, Chris Waits. (Hj)
*Blackfoot Valley Dispatch*, January 29, 1998, February 5(?),
1998, February 12, 1998.

35. (Qb) Written Investigator Reports.

36. (Qb) Written Investigator Report #79, Patrick
McIntosh, p.1.

37. Same, p.5.

38. Same, p.6.

39. Same, p.8.

40. (Qb) Written Investigator Report #77, John Masters, p.1.

41. Same, p.3.

42. Same, pp. 3, 4.

43. Same, p.5.

44. (Qb) Written Investigator Report #98, W.Pr., pp. 4, 5.

45. Same, p.5.

46. (Qb) Written Investigator Report #28, G.Da., p.4.

47. (Qb) Written Investigator Report #104, Roger
Podewell, p.3.

48. Jeanne En. lists these as the usual participants. See
(Qb) Written Investigator Report #33, K.H. and
Jeanne En., p.13. Dale Es. lists the usual participants as
himself, my brother, my parents, David and Shirley Hbr. (Qb)
Written Investigator Report #32, Dale Es., p.7. I had never
heard of David and Shirley Hbr. until I read this report. At
the one colloquium I attended, the participants were those
I've listed.

49. (Qb) Written Investigator Report #32, Dale Es., pp. 7,8.

50. (Qb) Written Investigator Report #33, K.H. and Jeanne
En., pp. 14, 15.

51. Same, p.10.

52. (Ca) FL #293, letter from David Kaczynski to me, October
1 or 2, 1984.

    In reference to the attitudes that my brother and the
En.'s held toward me at the time of Dan's suicide, it may be
worthwhile to quote also another letter of my brother's.
At some point during 1984, knowing that my brother was going
to visit K.H. and Jeanne, I sent him in care of them three
cartoons that I had drawn, with some humorous commentary in
Spanish. In reply Dave wrote me (Ca) FL #289, Summer,
1984, pp. 2-4:

    "I ended up having to translate your long letter...
\[It was\] well worth it in light of the jokes which dawned
on us in the process. I gathered that in your historiography
of boasts there was somewhat of a serious message as well.
Your humor is so inventive and so highly original that I
never cease to marvel at it, while at the same time finding
it a pity that it's restricted to such a small audience.
You asked me once whether K.H. and Jeanne are in any way
capable of being offended by coarse humor. Now I can tell
you that \[K.H.\] enjoyed the two cartoons which might have
been considered coarse immensely, whereas Jeanne's
reaction seemed rather complicated. ... \[S\]he pointed
out some very artful touches in your cartoons. And I found
myself very much in agreement with her. Have you ever
thought of trying to sell your cartoons to magazines? ...
I honestly and I believe without \[text unknown\] cartoons
on the average the most interesting I\'ve ever seen.\"

    This does not contradict in any specific way what K.H.
and Jeanne told the investigators about me, but it doesn't
comport very well with the image of me that they conveyed.

53. (Qb) Written Investigator Report #33, K.H. and
Jeanne En., pp. 7-10.

54. (Ca) FL #304, letter from me to David Kaczynski, late
spring or summer of 1985, p.1: "I was amused by the Mexican
comic book. (But you should have included a critical
analysis by \[K.H. En.\] explaining the hidden philosophical
messages.)"

    (Ca) FL #220, letter from me to David Kaczynski, August
28, 1979, p.2:

    "\[K.H.\] sent me a copy of a 'Red Sonja\*' comic
book (footnote: \*An absurd female hero), asserting that
'to imaginative minds it drips of philosophical lessons.'

    "In reply I sent him \[mimicking Nietzsche's style\]:

    "'I have no time** to listen to thy teaching,
Zarathustra,' said the small man, 'For I must mow my lawn
and tend my melons; I have no time to listen to prophecies.
I have no time to be an arrow of longing for the farther
shore.' (footnote: \*\*\[K.H.\] wrote that he would read
some Nietsche \[sic\], except that he had no time because
he was too busy mowing his lawn, tending \[melons; the rest
of this footnote is cut off on the Xerox copy that I have.\].)

    "'How then,' answered Zarathustra, 'hast thou time to
read the book of a naked harlot pretending to be a hero?
Knowest thou not that a dark cloud hangs over men and that
even now are falling one by one the heavy drops that herald
the lighting? What then signify thy lawnmower and thy
melons? Verily, thou art become as the last man.' Thus
spake Zarathustra. - Nietsche \[sic\], *Zarathustra*, part
5." (The footnotes were in the original letter. According
to Nietzsche, the "last man" is a despicable and
degenerate human type.)

    This is a sample of the way I used to tease K.H. about
his comic-book philosophy. I intended the teasing to be
gentle and humorous, but it may be that I wounded K.H.
without realizing it.

55. (Qb) Written Investigator Report #29, Peter L. Duren,
pp. 9,10.

56. (Qb) Written Investigator Reports #28, G.Da., p.2; #55,
John Je., pp. 1,2. Ray Janz's story was reported in (Hm)
*San Francisco Chronicle*, April 29, 1996; (Hn) *Chicago
Tribune*, April 16, 1996; (Ja) *Mad Genius*, p. 26. According
to all three of these reports, Janz stated that I used a
pocket protector.

57. (Qb) Written Investigator Report #32, Dale Es., pp. 1,2.

58. Same, p. 4.

59. (Qb) Written Investigator Report #134, Lois Skillen, pp.
3, 6-8.

60. Professor Eickelman reported to my investigators that
Harvard was attempting to recruit him. (Qb) Written
Investigator Report #34, Dale Eickelman, p.1.

61. (Hk) *Scientific American*, May, 1997, pp. 24, 28.
