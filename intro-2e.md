# INTRODUCTION

"A FRIEND says there are a lot of people who mistake their
imagination for their memory." ¹

\- Daily Oklahoman

I am very different from the kind of person that the media
have portrayed with the help of my brother and my mother.
The purpose of this book is to show that I am not as I have
been described in the media, to exhibit the truth about my
relationship with my family, and to explain why my brother
and my mother have lied about me.

In fairness I should acknowledge that my brother and mother
probably are not fully conscious of many of their own lies,
since they both are adept at talking themselves into
believing what they want to believe. Yet at least some of
their lies must be conscious, as we shall see later.

I consider it demeaning to expose one's private life to
public view. But the media have already taken away my
privacy, and there is no way I can refute the falsehoods
that have been propagated about me except by discussing
publicly some of the most intimate aspects of my own life
and that of my family.

Ever since my early teens, my immediate family has been a
millstone around my neck. I've often wondered how I had the
bad luck to be born into such a nest of fools. My relations
with them have been to me a constant source of irritation
and disgust and sometimes of very serious pain. For some
forty years my brother and mother leaned heavily on me for
the satisfaction of certain needs of theirs; they were
psychological leeches. They loved me because they needed me,
but at the same time they hated me because I didn't give
them the psychological sustenance they were looking for; and
they must have sensed my contempt for them. Thus their
feelings toward me were, and remain, strongly conflicting.
In my brother's case the conflict is extreme.

I certainly can't claim that my own role in the life of my
family has been a noble one. I had good justification for
resenting my parents, but instead of making a clean break
with them in early adulthood, as I should have done, I
maintained relations with them: sometimes was kind to them,
sometimes used them, sometimes squabbled with them over
relatively minor matters, sometimes hurt their feelings
intentionally, occasionally wrote them emotional letters
expressing my bitterness over the way they had treated me
and the way they had exploited my talents to satisfy their
own needs. With my brother too I should have broken off
early in life. The relationship wasn't good for either of
us, but it was much worse for my brother than it was for me.
This is a complicated matter that I will deal with at length
further on.

This book is carefully documented. It has to be because
otherwise the reader would not know whether to believe my
account or that of my brother and mother. Due to the
continual need to quote documents and argue facts, the
writing is dry and perhaps pedantic. All the same, I think
the book will attract many readers because of the intrinsic
human interest of its content.

The amount of material about me that has appeared in the
media is enormous, and I have not read or seen more than a
small fraction of it. Apart from some straightforward
reports of legal maneuvers or courtroom proceedings, most of
what I have seen is loaded with errors and distortions, some
of them trivial, some of them very serious indeed. Due to
limitations on my own time, energy, and resources, the
documents I've studied in preparing this book include from
the media only a few items; principally the articles on my
case that appeared in *Newsweek*, *Time*, *U.S. News* and
*World Report*, and *People* on April 15th and 22, 1996; the
"quickie" books that appeared within a few weeks after my
arrest, *Mad Genius* and *Unabomber*, the articles based on
interviews with my brother and mother that appeared in the
*New York Times*, May 26, 1996, in the *Washington Post*,
June 16, 1996, in the *Sacramento Bee*, January 19, 1997;
and my mother's and brother's appearance on *60 Minutes*,
September 15, 1996. The latter cover all of the public
statements about me made by my brother and my mother that I
have seen up to the present date, March 5, 1998. (Added
April 1, 1998: I've recently been reminded of some other
remarks by my brother, brief ones that have appeared in
various newspapers, but I don't think they contained
anything that I need to address in this book.)

Apart from the published sources, I cite a large number of
unpublished documents. It will of course be necessary at
some point to make these documents accessible for
examination so that it can be verified that I have cited
them accurately. But I don't expect to do this immediately
on publication of this book. For one thing, some of the
documents are still legally sensitive, and for another, I
don't want journalists rummaging through my papers to get
material for sensational articles. I hope to get the
documents housed in a university library and arrangements
will be made so that some responsible and unbiased party can
examine them and verify that I have cited them correctly and
have not unfairly taken any passage out of context.
Eventually some of them will be published. In any case, I
will make every effort to see that the citations can be
independently verified at the earliest possible time.

I also make use in this book of a few reports received
orally from investigators who worked for my defense team.
The investigators do not want their names revealed because
the resulting publicity about them might interfere with
their work as investigators. But at some point I expect to
make arrangements so that the investigators can be consulted
discreetly and confirm the oral information that they gave
me. (But see below for my remarks on the reliability of this
information.) In this book I refer to the investigators as
Investigator #1, Investigator #2, etc.

Similar remarks apply to the psychologist whom I call Dr. K.

Needless to say, I am not able to provide documentary
evidence to refute all of the false statements that have
been made about me, or even all of those that have been made
by my brother and my mother. But I am able to demonstrate
that informants have been lying or mistaken in enough cases
to show that statements made about me are so unreliable that
they should not be given any credence unless they are
corroborated by documents written at or near the time to
which they refer.

In many cases I cite documents written by myself -
principally my journals, some autobiographical notes, and
letters sent to my family. All of these were written at a
time (prior to my arrest) when I had no motive to lie about
the points that are now at issue.

They were either seized by the FBI when they searched my
cabin, or were in the custody of other persons at the time
of my arrest. Since my arrest I have not had physical
possession of any of these documents; I have worked from
Xerox copies. Thus there can be no question of my having
fabricated any of this material for the purposes of this
book. (Exception: Notes that I took on information given to
me orally by the investigators and by Dr. K. were of course
written after my arrest and while I was preparing this
book.) Moreover, some of these documents, especially my 1979
autobiography, contain highly embarrassing admissions that
show that I was striving to be as honest as possible. Some
of the documents were written almost immediately after the
events that they record; others, while not contemporary with
the events, were written many years ago when my memory of
the events was fresher, and hence they presumably provide
more reliable evidence than someone else's recollections
taken down within the last year or two.

In many cases I make use of sources of information that I
know to be unreliable, such as media reports. The rationale
for doing this is that if the reader has conceived a certain
impression of me from unreliable sources, and if I can show
by quoting those same sources that the impression is not to
be trusted, then I will at any rate have demonstrated that
the sources are unreliable and hence that the reader has no
reason to believe them. As for statements of my brother and
my mother that were quoted in the *New York Times*, the
*Washington Post*, and the *Sacramento Bee*, my mother and
brother presumably saw the articles based on their
interviews, and, as far as I know, they never wrote letters
to the newspapers in question correcting any errors, so they
have to be considered responsible for their statements as
quoted in the articles.

In all cases when I have felt that a source was more or less
unreliable, I have warned the reader of that fact in the
Notes on Documents.

Quite apart from the unreliability of the media, I was
appalled to learn how few people provided trustworthy
information. A psychologist (Dr. K.) repeatedly interviewed
my brother, my mother, and me. She gave me orally some items
of information obtained from my brother, mother, and aunt,
and I wrote these down at the time. But when I asked her to
confirm some items of this information several months later,
in three cases out of a total of nine she either said she
couldn't remember any such information and couldn't find
it in her notes, or she reworded the information in such a
way as to change its meaning significantly. ² Other shrinks
misquoted me or gave seriously incorrect information in
their reports. The investigators who worked for my defense
team were much more reliable than the shrinks, but they too
gave me orally a few items of information that they later
had to correct, not because they had learned something new
from further investigation but because they had reported to
me carelessly in the first place. For this reason I have
tried to rely as little as possible on information received
orally. Wherever I have used such information the reader is
made aware of it either in the text or in a footnote and he
or she is advised to receive such information with caution.
I have cited oral information from Dr. K. or the
investigators in only a few cases. It is possible that
Dr. K. or the investigators may decline to confirm some of
this information if they are asked. Yet I was careful in
recording the information and I am certain that I have
accurately reported what I was told.

What really horrified me, though, was the nonsense reported
to the media or to the investigators by people who knew me
years or decades ago. The investigators have given me
written reports of interviews conducted with approximately
150 people. ³ Some of the information obtained in these
interviews dealt with matters of which I have no knowledge,
hence I am unable to give an opinion of its accuracy. Taking
into consideration only matters of which I have knowledge
and speaking in rough terms, I can say that something like
14% of the informants gave reports the accuracy of which I
was unable to judge; 6% gave reports about whose accuracy I
was doubtful; 6% gave reports that were inaccurate in detail
but provided an overall picture of me that was
not far from the truth; 36% gave reports that
were fairly accurate; 38% gave reports that were seriously
inaccurate; and, of these last, eleven persons gave reports
that were so far off that they were mere flights of fancy.
More than that: of the reports that were fairly accurate,
72% were brief (one and a half pages or less); while fewer
than one in four of the seriously inaccurate reports were
brief. So it seems that people who spoke carefully and
responsibly usually didn't have much information to give,
while most of those who had (or thought they had) a good
deal of information didn't know what they were talking
about. (I was told that under normal circumstances the
investigators would have interviewed the subjects over and
over in order to separate the wheat from the chaff, but for
some reason this was not done in my case.)

To judge from what I have seen of them, statements about me
made to journalists by people who knew me, as quoted in the
media, were even more inaccurate than what was reported to
my investigators.

In some cases I have documentary evidence that shows that
reports about me are false, but in the great majority of
cases I am relying on memory for the information that
disproves the reports. Why do I assume, when my
recollections disagree with someone else's, that mine are
usually right?

*First:* In many cases I can be confident that I am right
simply because I am in a better position to know about the
matter in question than are the persons whose memories
disagree with mine. For instance, if someone says that I
used to wear a plaid sport-jacket four decades ago, I can
safely assume that he has me mixed up with someone else,
because I have owned very few sport-jackets in my life and I
know that I have never had a plaid one.

*Second:* I have good evidence of the accuracy of my
long-term memory. ⁴

\(A\) lnvestigators working for my defense team who
researched my past told me repeatedly that my long-term
memory was remarkably sharp and accurate. ⁵ This does not
mean that I *never* made mistakes of memory, but that I did
so seldom. See Appendix 11.

\(B\) In preparing this book I've studied hundreds of old
family letters ⁶ that my mother had saved, going all the way
back to 1957, and I've found hardly anything to surprise
me: to the extent that the matters covered in the letters
overlapped with areas of which I have memories, my memories
were confirmed with only minor discrepancies.

\(C\) During the 1990's, for reasons that I need not take
the trouble to explain here, I obtained from Harvard a
transcript of my record. Before looking at it, as a check
on my memory, I wrote down on a sheet of paper the
number-designations of the courses I took (e.g. "Math 1a")
and the grades I got in them. The FBI found this sheet of
paper in my cabin and I have a copy of it. ⁷ Here is how it
compares with the official transcripts ⁸ of my record:

General Education AHF (which everyone referred to as "Gen
Ed A"), Humanities 5, and Social Sciences 7 were courses
lasting two semesters; all other courses were of one
semester.

<"INSERT TABLE HERE">

As far as I can recall, I never saw a transcript of my
Harvard grades from the time I left Harvard in 1962 until I
wrote them down from memory in the early 1990's.

\(D\) In the other surviving documents I have found
reasonably good agreement with my memories. When I have
encountered a discrepancy between my memories and someone
else's memories as reported in the media or to my
investigators, and when some document was available that
resolved the discrepancy, the discrepancy has always been
resolved in my favor, with very few exceptions. ¹² (However,
I can think of two cases - one trivial, one significant - in
which my memory has disagreed with someone else's and I am
sure that the other person is right because the matter is
one about which she could hardly be mistaken. ¹³ Also, when
I recall things that I have read years previously in books
and magazines, it is not uncommon for my memory of what I
have read to be distorted; occasionally it is seriously
wrong. ¹⁴ On the other hand, my memory of things I have
written or read in personal letters or heard in conversation
seems to be pretty reliable, so far as surviving documents
have made it possible to judge.)

*Third:* There is abundant evidence of the gross
unreliability of the memories of me that have been reported
to my investigators or have appeared in the media. In
reference to the information given to the investigators,
Investigator #2, who is very experienced, writes:

"Lay witness reports of Ted's behavior and functioning are
extremely suspect given the high profile nature of his case.
Many of their anecdotes and conclusions are most likely the
result of planted memories and suggestions they've read,
seen, or heard from others." ¹⁵

There are three ways by which I have been able to establish
that they are wrong. They may contradict information
about which I am in a position so well that there is
hardly any chance that my own memory could be mistaken; they
may contradict convincing documentary evidence; or the
accounts of two different people may contradict one another,
so that at least one of them must be wrong.

Throughout this book the reader will find examples of
reports that are proved wrong. But it will be useful to give
some examples here in the Introduction also, because, among
other things, they will illustrate some of the ways in which
false memories or false reports arise.

Some of the sources of falsehood or distortion can be
identified with reasonable confidence: (a) Media planting.
The informant "remembers" something because it has been
suggested to him by the media. (b) Mistaken identity. The
informant has me mixed up with someone else. (c) Remembering
later years. The informant remembers the later years of his
association with me, largely forgets the earlier ones, and
attributes to the earlier years the same traits,
relationships, or circumstances that existed in the later
years. (d) Stereotyping. The informant sees that I have some
of the traits of a given group, so he identifies me with
that group and assumes that I have all of the traits that
are characteristic of it. (e) Lying. It is difficult to say
how many of the falsehoods told about me are conscious lies.
At least some of the things that my brother and my mother
have said are conscious lies and not honest errors, and I
can identify one other individual who definitely has been
lying about me. But otherwise my guess is that the conscious
lying by *informants* has not played an important role; it is
a matter, instead, of human fallibility and irrationality.
On the other hand, some conscious lies by journalists can be
clearly identified, and there is enough evidence of
unscrupulousness and irresponsibility in the media to make
it plausible that journalists may often lie when they think
they won't get caught.

Apart from the factors we've just listed there are four
others that may have helped to produce false reports in my
case, but their existence is more-or-less speculative and
cannot be definitely proved. These are: (f) Projection.
People who themselves have mental or psychological problems
are prone to see others as having such problems. (g)
Personal resentment or jealousy. This factor is clearly
present in the case of my brother and mother. In some other
individuals its presence may be suspected, but this is
speculative. (h) Mass hysteria, herd instinct. Under certain
conditions, when an individual or a class of individuals
within a society is pointed out as evil or worthy of being
cast out, an atmosphere develops in which other members of
the society draw together defensively, gang up on the
rejected person(s), and take satisfaction in reviling him or
them. It becomes something like a fad. Possibly sadistic
impulses are involved. Some such factor seems to be
operating in my case, but it is difficult to prove this
objectively. (i) Greed. Several people who once knew me have
appeared on television in connection with my case, and I
know of at least one person who was paid for it. Obviously,
those who told the most bizarre or exaggerated stories about
me would be most in demand by talk shows and therefore might
make the most money. When interviewed later by my
investigators, they would give them the same story that they
gave on television so as not to have to admit to themselves
or others that they had perhaps allowed their memories to be
warped by greed.

Now some examples:

\(a\) *Media planting*. There are very many instances in
which I am reasonably sure that this has occurred, ¹⁶ but
often I can't prove it definitely. For example Leroy
Weinberg, a neighbor of ours when I was a teenager, told
investigators that when he said "hello" to me I always
failed to respond. ¹⁷ I know that this is false, because my
mother had me well trained to be polite to adults, and that
included answering all greetings from them. ¹⁸ It seems
fairly obvious that Weinberg attributes this and other
strange behavior to me because his memory of me has been
warped by exposure to the media; but how can I be certain?
Conceivably he might remember some instance in which I
failed to respond to a greeting of his because I simply
didn't hear it.

However, there are some cases in which it does seem
virtually certain that media planting has been at work.

Dr. L.Hz., a dentist who practices part of the time in
Lincoln, Montana, told my investigators: "Ted must not have
had much money because his mother usually paid his dental
bills.\" ¹⁹ My mother had provided me with a large sum of
money from which I paid my dental bills among other things,
but she never paid any of my dental bills directly. I
deposited her money in a bank and paid Dr. L.Hz. either in
cash or with checks on my own account. There is no way that
Dr. L.Hz. could have known that the money came ultimately
from my mother, because I was embarrassed about the fact I
received money from her, and I was careful to conceal it
from everyone. Certainly I would never have told Dr. L.Hz.
about it. It is clear, therefore, that Dr. L.Hz. must have
learned from the media after my arrest that I had been
receiving money from my mother, and this information altered
his memory of his own dealings with me.

Dr. L.Hz. also told my investigators: "Ted was an extremely
quiet person, so quiet that Ted appeared odd. Ted
was a kooky man. ... Ted did not talk much." ¹⁹
Media planting was probably involved here, too, as
Dr. L.Hz.'s account is contradicted by that of his own
dental assistant, R.Cb. According to my
investigators, R.Cb. "described Ted as, 'a sweet, nice,
pleasant guy.' ... She said that Ted was 'friendly' and she
would chat with him when he came into the office. She does
not remember what they talked about." ²⁰ Dr. L.Hz. was
present at most of my conversations with R.Cb. and he
participated in them.

Another clear example of media planting is provided by Dale
Eickelman, whom I knew in junior high and high school.
Eickelman, now a professor at Dartmouth College, told my
investigators that "Teddie did not have other friends
\[than Dale Eickelman\] during the time that Dale knew
Teddie from 5th grade until Teddie's sophomore year \[of
college\]." ²¹ In Chapter III of this book (pp. 79, 87, 88)
I mention eight people (other than Dale Eickelman), of
approximately my own age or up to two years older, with whom
I was friends during some part (or in one case almost all)
of the period between fifth grade and the time I left high
school. ²² These were good friends whom I genuinely liked,
not just casual acquaintances or people (like Russell Mosny)
with whom I spent time only because we were thrown together
as outcasts.

Professor Eickelman is a highly intelligent man. He must
realize that his house was a least a mile and a half from
mine, and that after fifth grade we were never in any of the
same classes at school. So how can he imagine that he knows
whether I had any friends other than himself? The only
evidence he cited was that when he visited my house (which
was not very often) no other friends were present. ²³ But it
was equally true that when I visited Eickelman's house he
never had any other friends there. Would this justify me in
concluding that his only friend was myself?

Professor Eickelman's belief that he was my only friend
clearly has no rational basis. Only one plausible
explanation for this belief presents itself. It was
suggested to him by the media portrayal of me as abnormally
asocial. It is true that I was unsuccessful socially in
junior high and high school. Thus the media did not create
Professor Eickelman's belief from nothing, but caused him
to exaggerate grossly the accurate perception that I was
less social than the average kid.

\(b\) *Mistaken identity*. In Chapter VI the reader will
find several examples of mistaken identity: cases in which
it can be clearly shown that an informant has made a false
statement about me because he has confused me with someone
else. We give another example here.

G.Wi. owns a cabin not far from mine, though I haven't seen
him for several years. According to investigators who
interviewed him, "[G.Wi.] thinks that Ted was always looking
over his shoulder. Sometime during the 1970's, Ted talked
to [G.Wi.] about the KGB. Ted told [G.Wi.] he had a place
he could hide in up \[sic\] Old Baldy where no one would
ever find him." ²⁴

G.Wi. has me mixed up with Al Pinkston, a gentleman whom he
and I met up in the Dalton Mountain or Sauerkraut Creek area
about late December of 1974. Pinkston (now deceased) was an
obvious paranoiac who believed that the Lincoln area was
infested with KGB agents. He told me he was hiding out up on
the mountain because "they're gunnin' for *my* ass." I
related the story of this encounter three months later in my
journal ²⁵ and in a letter to my parents. ²⁶

I never told G.Wi. or anyone else that I had a hiding place.

In this and in some other cases of mistaken identity, it is
likely that media influence was at work. G.Wi. probably
confused me with Al Pinkston because the media had portrayed
me as crazy, like Pinkston.

\(c\) *Remembering later years*. In greater or lesser degree
this phenomenon seems to affect a number of the reports made
to my investigators by people who have known me. In some
cases it is clear-cut. For example, Russell Mosny reported
that he and I met through our membership in the high school
band, ²⁷ but actually I knew him from the time I entered
seventh grade. ²⁸

In some cases it is difficult to disentangle the effect of
"remembering later years" from that of "media planting."
Thus L.D., the daughter of one of my father's best friends,
told investigators: "Ted Jr. was a very shy and quiet boy.
He was introverted and only involved himself in things he
could do alone." ²⁹ Here and throughout her interview, L.D.
exaggerates my shyness and introversion to the point of
caricature. Most likely this is the result of media
planting. Yet "remembering later years" would seem to be
involved too, since L.D. appears to have forgotten
completely the earlier years when I was not particularly shy
or introverted and we were lively playmates. I wrote the
following in 1979:

"I might have been about 9 years old when the following
incident occurred. My family was visiting the D\_\_\_\_
family. The D\_\_\_\_'s had a little girl named L\_\_\_\_,
about my own age. At that time she was very pretty. I was
horsing around with her, and by and by I got to tickling
her. I put my arms around her from behind and tickled her
under the ribs. I tickled and tickled, and she squirmed and
laughed. I pressed my body up against hers, and experienced
a very pleasant, warm, affectionate sensation, distinctly
sexual. Unfortunately, my mother caught on to the fact that
our play was beginning to take on a sexual character. She
got embarrassed and told me to stop tickling L\_\_\_\_.
L\_\_\_\_said, 'No, don't make him stop! I *like* it!' but,
alas, my mother insisted, and I had to quit." ³⁰

The most important case of "remembering later years"
involves my father's close friend Ralph Meister. On February
2, 1997 Dr. Meister signed for my investigators a
declaration in which he outlined what he knew about me and
my family life. The declaration is mostly accurate except in
one respect. Dr. Meister represents my mother and me as
showing certain traits through the entire period of my
childhood and adolescence, whereas in reality those traits
were not shown until I was approaching adolescence. Thus, he
writes: "Wanda put pressure on Teddy John to be an
intellectual giant almost from the day he was born." ³¹
Actually I never felt I was under much pressure to achieve
until at least the age of eleven. Dr. Meister also implies
that I had difficulties with social adjustment from early
childhood, ³² whereas in reality those difficulties did not
begin until much later. All this will be shown in Chapters I
through V of this book.

\(d\) *Stereotyping:* The most clear-cut example of this is
that some people remember me as having used a pocket
protector in high school. ³³ I have never used a pocket
protector in my life. But because I was identified with the
"Briefcase Boys" (academically-oriented students), and
because some of these did wear pocket protectors, people
remember me as having worn one too.

\(e\) *Lying:* Apart from my brother and my mother, the only
informant whom I definitely know to be consciously lying is
Chris Waits of Lincoln, Montana. Waits has been pretending
that he knows me well. ³⁴ He used to say hello to me when he
passed me on the road in his truck, and I would return his
greeting. I don't remember ever accepting a ride from him,
but it's conceivable that I may have done so on one or two
occasions, not more. I once had a brief conversation with
him at a garage sale. Apart from that I had no association
or contact with him.

One wonders what Waits' motive might be. Perhaps he is one
of those pathetic individuals who feel like failures in life
and try to compensate by seeking notoriety through tall
tales that they tell about some news event that has come
close to them. I recall that back in the 1950's there was a
derelict in Chicago named Benny Bedwell who "confessed" to
a highly publicized murder just in order to make himself
famous.

\(f\) *Projection.* It does appear to be true that persons
who themselves have mental or psychological problems are
prone to see others as having such problems, but it is
difficult to say definitely that this factor has operated in
my case, since the people who portrayed me as strange,
abnormal, or mentally ill may have done so under the
influence of "media planting" or some other factor. But it
is a fact that many of the people who portrayed me in this
way had serious problems of their own. For the case of Joel
Schwartz see Chapter XII and Appendix 6. Many other examples
can be found in the investigators' reports of the
interviews that they conducted. ³⁵ Here I will only discuss
some of my suitemates from Eliot N-43 at Harvard who gave
false information about me.

W.Pr., Pat McIntosh, John Masters, and K.M. formed a
close-knit clique within the suite. To all outward
appearances they were thoroughly well-adjusted. They wore
neatly-kept suits and ties, their rooms were always tidy,
they observed all of the expected social amenities, their
attitudes, opinions, speech, and behavior were so
conventional that I found them completely uninteresting. Yet
three of the four gave my investigators a glimpse of their
psychological problems.

Pat McIntosh, according to the investigators' report, did a
great deal of whining throughout his interview about how
hard it was to survive academically and psychologically at
Harvard. For example: "\[Pat\] found life at Harvard to be
extremely difficult. . . ³⁶ Patrick \[had\] his own
adolescent insecurities . . . ³⁷ Patrick was too insecure
and wrapped up in his own problems . . . ³⁸ The faculty or
administration at Harvard was . . . unconcerned with
students' emotional and psychological problems. Patrick did
not know any students who actually sought and received
emotional help... At times, Patrick wanted help surviving
himself, but he had no idea where to go. John Finley, the
house master. . . didn't want to recognize the serious
difficulties that many of the students were having." ³⁹

McIntosh evidently assumes that I was having problems
similar to his own: "One day during Patrick's second year
at Harvard . . . he saw a student being taken out on a
stretcher. The student had slit his wrists after receiving a
C on an exam . . . Patrick . . . thought of Ted and worried
that maybe Ted might end up like this kid." ³⁸

John Masters told the investigators that he "was two years
old when the United States dropped the atomic bomb on
Nagasaki and Hiroshima. After that he used to dream
about the atomic bomb; these dreams sparked John's
desire of becoming a nuclear physicist but after he barely
earned a C in his freshman physics class at Harvard, he
decided that he was not cut out for a career in the hard
sciences. . .  ⁴⁰ During John's first semester of his
sophomore year at Harvard, his family began to fall apart.
He became very depressed for several months and started
receiving therapy at the student health services". ⁴¹

When John Masters first moved into Eliot N-43 he mentioned
having been in "the hospital." I asked him what he had
been in the hospital for, and he answered, "just
nervousness." Like McIntosh, Masters made false statements
about me and exaggerated my solitariness. According to the
investigators' report of his interview, "House Master
Finley. . . did not intervene on John's behalf when John
needed counseling. The same was probably true for Ted.
Ted's solitary nature failed to draw Master
Finley's attention because diversity or unusual
behavior was accepted at Harvard. John believes that
today Ted's solitary behavior would warrant some type of
intervention; at the time, his behavior did not even
raise an eyebrow. ⁴² . . . John's solitary lifestyle meant
that he did not make more than five friends while at
Harvard." ⁴³

W.Pr. "was shy and socially backward when he went to Harvard
and feared that he would never fully come out of his shell.
. . . He had a strong desire to lead a normal life. W.Pr. was
an astronomy major. He originally intended to pursue
astronomy on the graduate level but his fears drove him away
from that goal. He saw that many of the astronomy graduate
students at Harvard were not well-adjusted and he felt he
would move further away from a normal life if he pursued
astrophysics.

"At the end of W.Pr.'s junior year, he dropped out of
Harvard. He was confused as a college student and this
confusion led him to drop out of school. [W.Pr.] went to
the Harvard health services for counseling before dropping
out of Harvard. He thought the counseling was helpful. . .
he returned to Harvard a year or two later. W.Pr. did not
last long at Harvard and soon dropped out again." ⁴⁴

W.Pr. too made false statements about me and exaggerated my
solitariness. "W.Pr. and the others at N-43 were too young to
realize how serious Ted's isolation was for him. . ." ⁴⁵

Thus McIntosh, Masters, and W.Pr. appear to have seen me as
having problems or needs that were, in part, similar to
their own. In reality I was psychologically self-reliant and
felt neither insecure, nor depressed, nor did I feel in need
of help, nor did I find it hard to face the academic
challenges of Harvard. Nor did I feel troubled by
loneliness. I did suffer from acute sexual starvation: I was
in daily contact with smart, physically attractive Radcliffe
women and I didn't know how to make advances to them. I did
feel very frustrated at a few mathematics teachers whose
lectures I considered to be ill-prepared. Apart from that
there was just one other thing about which I felt seriously
unhappy: It was a kind of nagging malaise the nature of
which I never fully understood until I broke free of it once
and for all in 1966. But that is a story that will be told
elsewhere than in this book.

\(g\) *Personal resentment or jealousy.* Only in the case of
my brother and mother can resentment or jealousy be clearly
identified as a factor influencing reports given to
investigators. However, this factor may be suspected in some
other cases. Ellen A. (see Chapter VI) once told me that
"everyone" was jealous of me, presumably referring to the
people whom we both knew, including G.Da. and Russell Mosny,
both of whom seemed to become cool toward me at about the
time I moved a year ahead of them in school. In G.Da.'s
opinion, "Academically and intellectually, Ted was head and
shoulders above the rest of the students at Evergreen Park
High. His exceptional intelligence set him apart, even from
a group of bright young men like the Briefcase Boys." ⁴⁶ 
"The Briefcase Boys" was a clique that included, among
others, G.Da., Russell Mosny, and Roger Podewell. According
to Podewell, "It wasn't just Ted's shyness that set him
apart from the Briefcase Boys. He was more intelligent than
the others, a fact that made Roger a little jealous . . .
." ⁴⁷ G.Da. and Mosny both went to the University of
Illinois and flunked out. Roger Podewell went to Yale and
got a C average his first year. (How he did after that I
don't know.) I did not fail to josh Podewell and Mosny
about their academic performance, but they didn't seem to
find it amusing.

G.Da., Podewell, and Mosny (especially the last) gave my
investigators unflattering and inaccurate accounts of me
that exaggerated my social isolation. Is this due only to
media planting or are dislike, resentment, or jealousy also
involved? My guess is that no such factor is involved in
Podewell's case but that it is involved in Mosny's.
With G.Da. it could be either way.

"Patrick \[McIntosh\] was jealous of Ted's prowess in
mathematics... ." ³⁹ Did this influence McIntosh's highly
inaccurate and unflattering portrayal of me? There is no
proof that it did. But it's a fact that a sense of
inferiority can be one of the most powerful impulses to
resentment. Especially when the person who appears to be
more able is lacking in tact, as I'm afraid has sometimes
been the case with me.

\(h\) *Mass hysteria, Herd instinct*. This is a very
vaguely-defined factor that has probably been at work in my
case, but it is impossible to separate from media planting
or illustrate with specific examples.

\(i\) *Greed.* Although I know of at least one case of a
person receiving payment for an interview, I have no way of
proving that people who told stories about me on television
allowed themselves to alter their recollections in such a
way as to make them more profitable financially. But it is
worth noting that two of the people who appeared most on
talk shows - Russell Mosny and Pat McIntosh - gave my
investigators accounts of me that were among the most
exaggerated and inaccurate.

\*\*\*\*\*\*

Let us conclude with a few more examples that show the
unreliability of the reports made to investigators by
people who have known me.

My brother used to hold literary "colloquia," as he called
them. He and a few friends would all read some piece of
literature that one of them had selected, then they would
get together and discuss it. The participants varied, but
the most usual ones were my brother, my parents, Dale E.,
K.H. and Jeanne E. ⁴⁸ I attended one and only one of these
colloquia. This was shortly after I arrived at my parents'
home in Lombard, Illinois in 1978. To the investigators
Dale E. described my behavior at this colloquium as follows:

"On the first occasion Date met Ted, Wanda and Ted Sr. \[my
father\], Dave and he were discussing Plato, in connection
with something they had read in their book club. Ted came
out of his room and said there was no reason to read any
early Greek philosophers like Plato because they had all
been proven wrong. That was all Ted said before returning to
his room or leaving the house. . . . \[Ted\] never made eye
contact, but just looked off blindly while he spoke." ⁴⁹

Here is how Jeanne E. described my behavior at the same
colloquium:

"\[Jeanne met Ted\] one night when she and K.H. were back at
the Kaczynskis' house for another colloquy \[sic\]. When he
was introduced to her, Ted made a disparaging comment about
her and about women in general. She was completely shocked,
but the nature of Ted's comment made her feel that there
was no point in trying to get to know Ted. Later, when the
group began the colloquy Ted participated at first, but
Jeanne recalls that he soon disagreed with something in the
discussion. He then became nervous and fidgety and kept
getting up, walking out and coming back to the
conversation." ⁵⁰

The reader will observe that the two accounts are
inconsistent with one another. At least one of them must be
false.

As a matter of fact, both are false. I remember the
colloquium quite clearly. The participants were Dale E.,
K.H. and Jeanne E., my parents, my brother, and myself. I
can state exactly where each of us was sitting, I can
describe in a general way the demeanor of each, and I can
even recall some of the details of the conversation. The
subject of the colloquium was a dialogue of Plato that
discussed happiness and love; Plato's conclusion was that
true happiness lay in the love of wisdom.

I was present in the living room when the others entered. I
did not make a disparaging comment about Jeanne personally.
I did not make a disparaging comment about women in general
when I was introduced to Jeanne, but it is conceivable that
at some later point I may have made a comment about women
that might have been felt as disparaging by a woman who was
excessively sensitive about her gender. However, it's more
likely that Jeanne is remembering a joking comment
about women that I made in a letter to her husband, K.H.,
during the mid-1980's, (Added July 20, 1998: Since writing
the foregoing, I've obtained copies of some of my letters
to K.H. including the letter mentioned here. This undated
letter refers jokingly to "Woman, the vessel of evil.").

I did not say that the early Greek philosophers had "been
proven wrong." I did say that their methods of reasoning
were naive by modern standards, hence they were worth
reading today only for esthetic reasons or because of their
historical interest, not as a source of rational
understanding.

I did not become "nervous" or "fidgety", and I did not
leave the room at any time until all of the guests had left.
I did repeatedly get up to take pieces of snack food from a
bowl that was on a table five or six feet from where I was
sitting. It is probably some garbled memory of this that
leads Jeanne to say that I kept getting up and walking out.

Dale E.'s statement that I "never made eye contact" with
him is literally true, but it was he, not I, who avoided eye
contact. I looked at Dale E.'s face a number of times during
the evening, but he never looked back at me. I'm more than
willing to put the matter to a test. I invite Mr. E. to come
and visit me in the presence of witnesses. Let the witnesses
judge which of us has difficulty maintaining eye contact
with the other.

Besides his evasion of eye contact, Dale E. seemed unable to
deal with any challenge to his opinions. Twice during the
evening I made so bold as to disagree with him. In each
case, instead of answering my argument, he just shut his
mouth, elevated his nose, and looked away without saying
anything.

K.H. didn't give the investigators any account of my
behavior at the colloquium, or at least none is mentioned in
the report that I have. He did have much else to say about
me, however, and it is mostly fantasy. Unfortunately, no
documents are available that confirm or refute his
statements except in one case. According to the
investigators' report of their interview with K.H. and
Jeanne.

"\[K.H.] and Jeanne compared Ted to Jeanne's brother Dan who
was severely mentally ill and killed himself in 1984. In
fact, Dave \[Kaczynski\] also knew Dan and saw a clear
parallel between Dan and Ted. Dan had extremely rigid
opinions and was often 
intolerant and impatient of divergent views. ... Dave, in
fact, found Dan and Ted so similar that when Dan finally
killed himself in 1984, he began to worry that Ted might do
the same." ⁵¹

But here is what my brother wrote to me in 1984, shortly
after Dan's suicide:

"I've been feeling kind of depressed the last couple of
weeks since learning that Jeanne's brother Dan committed
suicide. As he lived with K.H. and Jeanne, and didn't have
a regular job, I spent quite a bit of time with him during
my two visits in Rockport. We... often talked about
philosophy. ...

"\[I\]t was hard getting through to Dan. On the other hand,
he seemed to have a message he was trying to get across, and
which he didn't feel that I, K.H., or anyone had yet
appreciated adequately. So he must have felt a similar
frustration with *us*, in answer to which, according to
K.H., he seemed to be withdrawing from everyone more and
more during the last couple of years. K.H. seemed to think
that Dan's suicide was a 'rational act' - i.e. that it was
a consequence of his ideas. The arresting thing for would-be
intellectuals, such as K.H. and me, assuming this were true,
is the facility and resolution with which Dan's 'idea'
translated itself into an act. \[K.H.\]... is even worse than
me, living a bourgeois \[sic\] lifestyle in almost all
respects except his reading.

"... When I spoke to \[K.H.\] on the phone, he still sounded
unusually distraught. If Dan had intended at all to make a
permanent, life-long impression on \[K.H.\] - to break through
the barrier of mere philosophizing at last - then I think
he might have succeeded. The rest of the family prefers - I
suppose for obvious reasons - to interpret Dan's later years
and his suicide as symptoms of a mental disease. ... \[Dan's
death\] reminded me of the sometimes dismal gulfs which
isolate human beings from one another. It reminded me just a
tad of myself, having ideas and affections, but often
feeling at a loss for the proper means to share them. More
acutely, I felt somewhat guilty, as if I were being called
to account for my unresponsiveness to similar claims made on
me by others." ⁵²

In his interview K.H. goes on and on about my supposed
"intolerance" of other people's ideas (making, at the
same time, many false statements about my behavior). ⁵³
As a matter of fact, I never had more than a very little
philosophical or intellectual discussion with K.H. but
(though I was not knowingly tactless) that little apparently
was enough to show him that I did not respect him or his
ideas, which presumably is why he thought I was
"intolerant." If the reader were to make K.H.'s
acquaintance and familiarize himself with his ideas, he
would be able to make his own judgment as to whether my lack
of respect for them was due to intolerance or to the quality
of the ideas.

K.H. used to read children's comic books and claimed that
he found philosophical messages in them. ⁵⁴ I once asked him
whether he believed the messages were put there
intentionally or whether he created them himself out of the
comic-book material. He answered that he preferred not to
discuss the question at that time.

Among many other inaccuracies that appear in Professor Peter
Duren's interview with the investigators, there is the
following:

"The last time that Professor Duren ever saw Ted was at the
annual meeting of the American Math Society in San Francisco
in 1968. Ted did not give a talk which was strange since
professionally it was the right thing to do. Professor Duren
saw Ted standing near the escalator. He went over to talk to
Ted, and they had a very stiff, very brief conversation. The
conversation consisted of Professor Duren asking questions
that Ted did not feel like answering. Ted did not seem
comfortable or happy." ⁵⁵

This may be a case of mistaken identity or it may be just
fantasy. I was not a member of the American Mathematical
Society in 1968 and I have never in my life attended any
kind of mathematical meeting outside of a university where I
was a student or faculty member. I just wasn't that
interested in mathematics. I suppose the names of
participants in American Mathematical Society meetings are
recorded, and if that is so, then it may be possible to get
documentary proof that I was not at the 1968 meeting; but at
present I am not able to provide such proof.

\*

A few people reported that in high school I was once stuffed
in a locker by some "tough" kids and left there. ⁵⁶ If
this had ever happened, it wouldn't be the kind of thing I
would be likely to forget. Nor would I conceal it; I
reported other humiliating incidents in my 1979
autobiography, so why conceal this one? I'd guess that a
combination of media planting and mistaken identity are
involved here. Ray Janz, who told the story in the media, ⁵⁶
probably had me mixed up with someone else. Others, who knew
that *some* student had been stuffed in a locker, heard Janz's
story through the media and subsequently "remembered" that
I was the victim.

\*

In reference to my brother's years at Evergreen Park High
School, Dale E.'s . (who was one of Dave's teachers there)
told the investigators:

"Physically, . . . Dave was much smaller than his
classmates. He was also socially awkward. Dave was shy and
quiet and tended to keep to himself. Dale never saw Dave
hanging out with friends. . . . \[S\]ocially and physically,
he was behind \[his classmates\]. . . . Dave seemed socially
and physically awkward." ⁵⁷

Referring to the early 1970's, Dale E. said:

"Dave was still socially awkward and inept. . . \[W\]hen
Dale and Dave went for walks in the Morton Arboretum, Dave
made Dale walk ahead of him so that Dave did not have to
speak to any people they passed. He told Dale he did not
want to have to say hello to people." ⁵⁸

Lois Skillen, guidance counselor at the school, described my
brother during his high school years as follows:

"David was outgoing, friendly and sociable. . . . David had
friends and played sports. . . . David was outgoing and
happy. . . . David. . . sat down in the living room with all
the women and immediately started to chat with them. David
was laughing and having a good time. He was sweet, friendly
and social." ⁵⁹

The admirable consistency between Dale E.'s description of
my brother and Miss Skillen's should help the reader to
estimate the value of these reports.

Much of the information that Skillen gave my investigators
is inaccurate, but on this particular point she is right and
Dale E. is wrong. My brother is occasionally a little shy,
and he wasn't socially polished, but he never had any
trouble making friends. In high school, if anything, he was
more outgoing than he was later. I don't have Dave's
medical records, but they would probably show that he was at
least average height for his age. Anyone who thinks Dave is
physically awkward will soon change his mind if he plays
tennis or ping-pong with him. The Morton Arboretum incident
may well have occurred, since my brother occasionally
behaves a little oddly. But it does not fairly represent his
usual social behavior.

\*\*\*\*\*\*

It is interesting that there seems to be little relation
between the intelligence of an informant and the accuracy of
the reports that he gives about decades-old events. We've
seen that an adequate university professor like Dr. Duren
and an outstanding one like Dr. Eickelman ⁶⁰ were among
those who gave grossly inaccurate accounts of my early
years. Yet some people of modest intellectual attainments
have given accounts that are fairly accurate. I suppose it's
a matter of character. Some people refrain from speaking
when they aren't sure, whereas others seem to let their
imaginations run away with them.

I've shown that several factors have operated in producing
false reports about me, but I have little doubt that media
planting is the most important one. The fact that so many
people's memories of me have been warped as badly as they
have been shows the awesome power of propaganda.

*Scientific American* recently published an interesting
article on memory planting. ⁶¹ The phenomenon is not
hypothetical; its existence has been proved.

\*\*\*\*\*\*

This book deals only with the way I have been misrepresented
by my family and by the media. But the FBI, the prosecutors,
and the shrinks have misrepresented me just as badly, and I
expect to take them on in some later writing.
