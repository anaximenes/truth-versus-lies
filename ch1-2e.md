# CHAPTER I

I will begin with one of the biggest lies of all, a kind of
family myth manufactured by my mother.

I have only a vague recollection of the version of this
story that I heard from my parents in childhood. In essence
it was that as a baby I had been hospitalized with a severe
case of hives (urticaria), and that I was so frightened by
this separation from my parents that I was forever after
excessively nervous about being left alone by them.

It is not clear to me why my parents thought I was unduly
afraid of being separated from them. It may have been
because they became accustomed to being away from their own
parents at an especially early age - my mother's mother was
a drunken, irresponsible slut¹ who probably left her
children unattended on frequent occasions, and my father was
an extrovert who spent much of his childhood running with
gangs of boys rather than home (according to the stories he
told me). In any case, as I look back on it now, I don't
think I was any more anxious about being left alone than the
average kid of my age. When I was perhaps six or seven years
old, my mother began leaving me home alone for an hour or
two at a time, and I did not find it difficult to adjust to
this. At about the same age I once attended a movie with my
father in a strange neighborhood far from home, and after
the movie, he left me standing alone outside the theater for
ten or fifteen minutes while he went to get the car. I felt
a good deal of anxiety while waiting for him, but I think
not more than is normal for a kid of that age under such
circumstances. I certainly did not feel panicky nor did I
doubt that my father would return. He told me afterward that
he had left me alone in order to help me get over what he
called my fear of being away from my parents.

My parents retained their belief that I had an unusual fear
of being separated from them until I was thirteen years old.
At that age, I was sent away to summer camp for two weeks.
Though I was somewhat homesick, I had no serious difficulty
in adjusting to the experience, ² and after that, as far as
I can remember, my parents never again mentioned my supposed
fear of being "abandoned" by them - until many years
later, when my mother resuscitated the myth of "that
hospital experience" in exaggerated and melodramatic form.
Her motives for doing so will be explained in Chapter IV.
For the moment, I am concerned only to describe the myth
itself and to refute it.

Here is the myth in my mother's own words, from a letter
that she wrote to me on December 24, 1984:

"\[Your hatred of your parents\] I think, I am convinced,
has its source in your traumatic hospital experience in your
first year of life. You had to be hospitalized with a
sudden, very serious allergy that could have choked off your
breath. In those days hospitals would not allow a parent to
stay with a sick child, and visits were limited to one hour
twice a week. I can still hear you screaming 'Mommy, Mommy!'
in panic as the nurse forced me out of the room. My God! how
I wept. My heart broke. I walked the floor all night
weeping, knowing you were horribly frightened and lonely.
Knowing you thought yourself abandoned and rejected when you
needed your mother the most. How could you, at nine months,
understand why - in your physical misery - you were turned
over to strangers. When I finally brought \[you ³ \] home
you were a changed personality. You were a dead lump
emotionally. You didn't smile, didn't look at us, didn't
respond to us in any way. I was terrified. What had they
done to my baby? Obviously, the emotional pain and shock you
suffered those four days became deeply embedded in your
brain - your sub-conscious. I think you rejected, you hated
me from that time on. We rocked you, cuddled you, talked to
you, read to you - did everything we could think of to
stimulate you. How we loved you, yearned over you.  Some
said we spoiled you, were too lenient, doted on you too
much. But you were our beloved son - our first born and we
wanted so much to have you love us back. But I think that
emotional pain and fear never completely left you. Every now
and then throughout your life, I saw it crop up. ..." ⁴

I was surprised when I saw that in this letter my mother
described my hospitalization as having lasted only four
days. She had previously told me - repeatedly - that it had
lasted a week, ⁵ and that I had been "inert", "a dead
lump", for a month after I came home.

Here is what my brother reportedly said about "that
hospital experience" when he was interviewed by the FBI:

"TED had a severe allergic reaction and was hospitalized for
several weeks. His parents were only allowed short daily
visits and TED became unresponsive and withdrawn during his
stay in the hospital." ⁶

"When TED was a year or so old, he was hospitalized after
suffering a 'severe allergic reaction.' His parents were
restricted from visiting him for more than a few minutes a
day, and when he recovered and was taken home two or three
weeks later they noticed that he was markedly unresponsive
and displayed a significantly 'flat effect' (emotionless
appearance). It took weeks and even months for his parents
to re-establish a satisfactory relationship with TED, and
WANDA attributes much of TED's emotional disturbance as an
adolescent to this early trauma." ⁷

"DAVE stated that on four distinct occasions, TED has
displayed a type of 'almost catatonic' behavior which has
long perplexed and mystified his family. The first was his
withdrawal after a three-week hospital stay when he was an
infant." ⁸

Here is what my brother told the *New York Times*:

"David, who had been told the story by his parents, said
that the infant Teddy developed a severe allergy and was
hospitalized for a week. 'There were rigid regulations
about when parents could and couldn't visit,' David said.
He recalled that on two occasions, his parents 'were
allowed to visit him for one hour.'

"After Teddy came home, 'he became very unresponsive,'
David said. 'He had been a smiling, happy, jovial kind of
baby beforehand, and when he returned from the hospital he
showed little emotions \[sic\] for months.'" ⁹

*Newsweek* cited information from federal investigators (who
presumably were relaying information received from my mother
or my brother) as follows:

"The first clue is something that happened when Kaczynski
was only 6 months old. According to federal investigators,
little 'Teddy John,' as his parents called him, was
hospitalized for a severe allergic reaction to a medicine he
was taking. He had to be isolated - his parents were
unable to see him or hold him for several weeks. After this
separation, family members have told the Feds, the baby's
personality, once bubbly and vivacious, seemed to go
'flat.'" ¹⁰

*Time* gave a similar report. ¹¹

The FBI's "302" reports often contain inaccuracies, and
(as we will show later) journalists' reports are extremely
prone to gross inaccuracies that result from carelessness,
incompetence, or intentional lying. But the fact that
several different sources gave roughly similar accounts is a
good indication of the kind of information my brother and
mother had been giving out.

Furthermore, on April 12, 1996, Investigator #1, an
investigator for the Federal Defender's office at Helena,
Montana, interviewed my mother in Washington, D.C. According
to Investigator #1's notes, my mother gave her the story as
follows:

'When Ted was nine or 10 months old, he developed a severe
and sudden allergic reaction to something, his entire body
swelled, and he had severe itching all over. Wanda walked
with him the entire night, and took him to the University of
Chicago-Children's Teaching Hospital first thing in the
morning. She described the hospital visit as very traumatic
for both Ted and his mother. When they arrived, Ted was
taken from Wanda by a nurse and put in a separate room. Ted
started screaming and crying, calling nonstop for his
mother, who also started crying... . That Friday the
hospital called Wanda and said she could come and pick Ted
up, as the swelling had subsided. When Wanda arrived at the
hospital, she was handed her son, who she described as 'a
dead lump.' She said Ted would not respond to her or her
husband at all for weeks after the hospital stay. Wanda and
Theodore spent hours trying to bring Ted out of his shell,
coaxing a smile, or attempting to get him to play with a
toy, mostly without success. ...

"After the stay in the hospital, Wanda described Ted as
much more clingy, and less trusting of strangers. He would
scream whenever he was taken into a strange building,
fearful his parents were going to leave him. About four or
five months after Ted was released from the hospital, he
fell while running in the house, and split his tongue.
Wanda rushed him to the hospital, where he immediately began
screaming and fighting. ...

'Ted's regular pediatric visits were always upsetting, as
Ted acted terrified of doctors." ¹²

How accurate is this picture? Fortunately that question is
easy to resolve, because my mother kept a "Baby Book," or
diary of my development as an infant. The book contained
printed instructions and questions with blank spaces left
for the parent to fill in. (When quoting from the Baby Book,
I will put the printed matter in italics and material
written by my mother in ordinary type.) The following
excerpt from the Baby Book includes every word of my
mother's account of "that hospital experience," from the
first appearance of the symptoms to my apparently complete
recovery.

My age at the time was just over nine months.

"*FORTY-FIRST WEEK. Dates, from* Feb. 26 *to* Mar 5
\[1943\]

"Saturday, the 27th \[of February\] Mother noticed small red
splotches on baby's stomach and neck, as the day progressed
the splotches spread. In the evening we took him to the
hospital. The doctor diagnosed them as hives. Sunday
\[February 28\] the hives were worse but baby seemed not
effected \[sic\] by them. We took him for a long ride in his
buggy. Shortly after we returned we noticed the baby had a
fever. Called the hospital and was told to give him frequent
baths & 1/2 aspirin every 3 hrs. Monday morning \[March 1\]
the baby was examined at Bobs Roberts \[Hospital\] by
several doctors. The consensus \[sic\] of opinion was that
baby had a bad case of urticaria \[hives, rash\] & should be
left at the hospital. Wednesday \[March 3\], mother went to
visit baby. The doctors still think he has an extreme case
of urticaria but are not sure. The \[sic\] omitted \[sic\]
eggs from his diet. Mother felt very sad about baby. She
says he is quite subdued, has lost his abandoned virve
\[sic\] & aggressiveness and has developed an
institutionalized look.

"*FORTY-SECOND WEEK*. *Dates, from* Mar. 5 *to* Mar. 12
\[1943\]

"Baby's home from hospital. Perfectly healthy But quiet
and unresponsive after his experience. Hope his sudden
removal to hospital and consequent unhappiness will not harm
him.

"Later in the week - Baby is quite himself again. Vivacious
and demanding. Says 'bye-bye' by waving his hand.
\[Etc.\]" ¹³

According to hospital records ¹⁴, I was admitted on March 1,
1943 and released on March 6, so I was hospitalized for five
days. Since the statement that I was quite myself again
could not have been written later than March 12, it took me
*at most* six days (and possibly much less time) to make an
apparently complete recovery. It should also be noted that a
careful study of my medical records has turned up *no*
mention of my supposed unresponsiveness. Furthermore, on
September 6, 1996, my Aunt Freda (Freda Dombek Tuominen) was
interviewed in Gainesville, Florida by two investigators
working on my case. She told them that she was away on a
two-week vacation when I was hospitalized from March 1 to
6, 1943. When she returned, someone mentioned to her that I
had been in the hospital, but after that she heard nothing
more about the episode until it was publicized in the media
following my arrest. ¹⁵ Since Freda was very close to my
parents during the 1940's, this is a clear indication that
*at that time*, my mother did not attach much importance to
the hospitalization and that the effect on me was not
obviously serious.

What about my mother's statement that "Ted's regular
pediatric visits were always upsetting, as Ted acted
terrified of doctors?" ¹² That is another lie. The Baby
Book and my medical records show four, and only four,
instances in which I appeared to be afraid of doctors or
nurses, and two of these occurred *before* "that hospital
experience." Here are the corresponding entries from the
Baby Book and the medical records:

"*FIFTH WEEK. Dates, from* June 19 *to* June 26 \[1942\],

"... When the doctor was handling him today he cried a great
deal.  ... Perhaps he was frightened of the unfamiliar
surroundings and handling." ¹⁶

"*SEVENTEENTH WEEK. Dates, from* Sept. 11 *to* Sept. 18
\[1942\].

"... Sept. 15. When taken for his periodic
examination the child became very frightened of the
doctor." ¹⁷

In the medical records the two foregoing examinations are
recorded, but no mention is made of my reaction to the
doctor, ¹⁸ which probably indicates that the doctor did not
consider my reaction unusual.

My hospitalization occurred during the latter part of my
forty-first week. About a month later, the following
reaction was reported in the Baby Book:

"*FORTY-SIXTH WEEK. Dates, from* 4/2 *to* 4/9 \[1943\].

"This week we visited the hospital with Teddy. When mother
took him in to be undressed & weighed Teddy saw the nurses
in their white uniforms & immediately HOWLED. It's evident
he remembered his sojurn \[sic\] in the hospital. It took
about 10 min. for mother to calm him. When the doctor
entered the little room that he was taken to after being
weighed there was no definite reaction other than interest
in her, but as soon as she attempted to examine him he
yowled." ¹⁹

The hospital record of this examination does not mention my
fearful reaction. ²⁰

The last instance in which I showed fear of medical
personnel is mentioned in my medical records, but not the
Baby Book (which does not go beyond December 25, 1943):

"June 27, 1944. ... Reluctant to carry examination,
child is fearful of white coats since his visit for repair
of his tongue." ²¹

The reference is to an injury to my tongue ²² that had
occurred about two months earlier, on April 29, 1944. Note
that this extract from the medical records clearly implies
that prior to the tongue injury, I was *not* fearful of
medical personnel. That I was not afraid of doctors or
nurses for at least nine or ten months preceding my tongue
injury is confirmed by the absence of any mention in the
Baby Book or the medical records of any such fear on my part
between April 9, 1943 (about a month after my
hospitalization) and April 29, 1944 (the date of my tongue
injury), even though the medical records and the Baby Book
report that I was examined at the University of Chicago
clinics ²³ on May 18, 1943, June 13, 1943, October 19, 1943,
January 11, 1944, and January 18, 1944.  Moreover, the Baby
Book's one-year inventory of the child's development (late
May, 1943, less than three months after "that hospital
experience") includes the question, "*Does he* \[the
baby\] *show persistent fear of anything?*" My mother left
the question blank. ²⁴

*After* my tongue injury (which, by the way, did not require
hospitalization), my mother told a doctor that I was "quite
fearful of hospitals" (see extract below, April 4, 1945).
But that I had no *long-lasting* fear of doctors or
hospitals is confirmed by the following extracts from the
medical records ²⁵:

"June 13, 1943. ... Healthy w-d \[well-developed?\]
well nourished infant. No pathological findings."

(No mention of unresponsiveness or fear of doctors.)

"April 4, 1945... appetite excellent. Plays well
with other children. Quite fearful (?) of hospitals."

(Evidently the doctor is recording information furnished by
my mother. The question mark after "fearful" is in the
original and possibly indicates skepticism on the part of
the doctor. Further along in the report of this same
examination:)

"Sturdy, well nourished boy with good color who tries to
manipulate his mother by temper \[?\] outbursts. Submits
\[illegible\] but not quickly \[or quietly?\] to
examination - after she is sent from the room. Quite
agreeable at conclusion of examination."

(The foregoing entry contradicts my mother's claim that I
was afraid of being left by my parents, since the departure
of my mother calmed me and caused me to submit to the
examination.)

"January 4, 1946... A well nourished \[?\]
adequately muscled \[?\] very whiny little boy."

"April 10, 1946... A whiny but fairly cooperative
boy... ."

"October 16, 1947... A pleasant, quiet, alert,
slender boy... ."

"December 8, 1947... A friendly, intelligent
youngster who is not acutely ill. He is extremely
inquisitive of all that is said and requests explanations."

The foregoing include all of the passages in my surviving
medical records up to age 6 that have any bearing on my
behavior in the presence of doctors or nurses. So much for
my mother's claim that "Ted's regular pediatric visits
were always upsetting, as Ted acted terrified of doctors."

According to the *Washington Post*, "Ted had an almost
paralyzing uneasiness around strangers, a reaction, again,
that Wanda traced back to Ted's childhood
hospitalization." ²⁶

Apart from the few cases in which I showed fear of doctors
or nurses, the Baby Book reports two, and only two, cases in
which I was frightened by strangers, and both of these cases
occurred *before* my "hospital experience."

"*ELEVENTH WEEK. Dates, from* July 31 *to* Aug 7 \[1942\]

"Twice this week the baby was on the verge of crying when
approached by unfamiliar persons. After a bit of handling
and talking to by the strangers he became very friendly,
cooing and smiling in response to their overtures." ²⁷

How did I react to strangers (apart from doctors and nurses)
*after* the "hospital experience?" Only two pages in the
Baby Book provide relevant information. The one-year
inventory of the child's development instructs the parent:

"*Underline each of the following terms which seems
descriptive of the child's behavior. Doubly underline those
which are shown very frequently or in a marked degree* ... ."

The Baby Book then lists thirteen terms. One of them is
"shyness," and my mother underlined it once. (The other
terms are "curiosity," which my mother underlined doubly;
"excitability," "impulsiveness," "cautiousness,"
"jealousy," "stubbornness," "cheerfulness",
"sensitiveness," "boisterousness," all of which my
mother underlined once; and "irritability,"
"listlessness," "placidity," which my mother did not
underline at all. ²⁸ The same terms were listed in the
nine-month inventory, and there my mother underlined
"curiosity" doubly, she underlined "excitability,"
"impulsiveness," "stubbornness," and "boisterousness"
once, and she underlined none of the others. ²⁹)

Further along in the one-year inventory we find:

"*Does child show greater interest in children or in
adults? Describe*.  Either definitely likes or dislikes
adults Loves to tussle with other children *Is he usually
shy or friendly with strange women?* either
*men?* either *children?* friendly *Does he show any
special preferences for certain persons?* Yes
*Describe* For unaccountable reasons will either be very
friendly or unfriendly to strangers. But almost always
friendly to people he knows." ²⁸

About seven weeks after the "hospital experience" and
three weeks before the one-year inventory, we find in the
Baby Book:

"*FORTY-NINTH WEEK. Dates, from* 4/23 *to* 4/30 \[1943\].

"When the door buzzer rings Teddy, when in his walker,
immediately skoots \[sic\] to the door, no matter what he's
occupied with at the time. When not in the walker he insists
on being carried or assisted in going himself." ³⁰

Since I was so anxious to meet visitors, it's clear that I
had no particular fear of strangers and was not excessively
shy. The statement that I had "an almost paralyzing fear of
strangers" going back to my "childhood hospitalization"
is another lie.

Did my hospitalization at the age of nine months have any
lasting effect on my personality or behavior? I do not know
the answer to that question. But it is obvious that if the
experience tended to make me permanently fearful of doctors
or of strangers, or if it made me less social, then the
effect was so mild that it is not clear whether there was
any effect at all.

Psychologists consulted by my defense team searched the
literature for reports of empirical studies of children who
had suffered separation from their parents at an early age.
They found only one study ³¹ that was closely relevant to my
case. This study shows that my reaction to hospitalization
and my recovery from it were quite normal for an infant
hospitalized under those conditions. While the study found
that all "overt" effects of hospitalization in such
infants disappeared within 80 days, at most, and usually in
a fraction of that time, the infants were not observed for a
long enough period to determine whether there were any
subtler, long-lasting effects.

Thus it remains an open question whether my hospitalization
had any permanent effect on my personality. The aim of this
chapter has not been to prove that there could not have been
such an effect, but that whatever that effect may have been,
it was not what my mother and brother have described.

My mother's and brother's motives for lying about me will
be dealt with later. (See Appendix I for further evidence of
my mother's untruthfulness.)

\* \* \* \* \* \*

The passage from the Baby Book that describes my "hospital
experience" provides an example of the way the media lie.
In an article in the *Washington Post*, journalists Serge F.
Kovaleski and Lorraine Adams quoted the Baby Book as
follows:

"Feb. 27. 1943. Mother went to visit baby. ... Mother
felt very sad about baby. She says he is quite subdued, has
lost his verve and aggressiveness and has developed an
institutionalized look.

"March 12, 1943. Baby home from hospital and is healthy but
quite unresponsive after his experience. Hope his sudden
removal to hospital and consequent unhappiness will not harm
him." ³²

Compare this with the accurate transcription of the passage
given a few pages back. Kovaleski and Adams have made
important changes. On February 27 I was still at home. I was
not hospitalized until March 1, and the entry that Kovaleski
and Adams dated "Feb. 27" actually refers to March 3.
Kovaleski and Adams assign the date March 12 to an entry
that was obviously written earlier, and they completely omit
the entry that shows that on or before March 12 I had
already recovered completely from "that hospital experience".

Kovaleski and Adams altered not only the dates but also the
wording of the passage. The most important change was that,
where the Baby Book states that I was "quiet and
unresponsive," Kovaleski and Adams wrote that I was "quite
unresponsive." ³³

The effect of these obviously intentional changes is to give
the impression that the "hospital experience" and its
consequences were much more long-lasting and severe than
they really were.

## NOTES TO CHAPTER I

1. (Ae) Autobiog of Wanda, entire document. (Cb) FL
Supplementary Item #4, letter from my Aunt Freda to my
mother, October 1, 1986. Supported by oral communications
to me from my mother and my uncle Benny Dombek up to 1979.

2. (Ac) Autobiog of TJK 1979, p. 36: "I felt rather
homesick at this place, but not excessively so. I got along
alright." (Ab) Autobiog of TJK 1959, p. 5 has: "Up to
quite recently... I was very dependent on \[my
parents\] in that I became unhappy if far away from them for
any length of time, say a couple of days or more. Before
coming to Harvard \[at the age of sixteen\], I was greatly
afraid that I would suffer much from homesickness, but after
a couple of weeks of unhappiness, this no longer bothered me
at all. The ties seem to have snapped completely, as it no
longer bothers me at all to be away from home."

3. A small part of the original letter is missing here, but
it is clear from the context that the word "you" should
appear.

4. (Ca) FL #297, letter from my mother to me, December
24, 1984.

5. Both in (Ab) Autobiog of TJK 1959, p. 1 and (Ac)
Autobiog of TJK 1979, p.1, I gave the period of
hospitalization as a week. I could only have gotten that
information from my parents - probably my mother, since my
father rarely said anything about "that hospital
experience."

6.  (Na) FBI 302 number 1, p. 3.

7.  (Na) FBI 302 number 2, p. 6.

8.  (Na) FBI 302 number 3, p. 3.

9.  (Ha) *NY Times Nat*., May 26, 1996, p. 22, column 3.

10. (Hf) *Newsweek*, April 22, 1996, p. 29.

11. (Hg) *Time*, April 22, 1996, p. 46.

12. (Ka) Interview of Wanda by Investigator #1, pp. 1,2.

13. (Bc) Baby Book, pp. 111, 112.

14. (Ea) Med Records of TJK, U. Chi., March 1-6, 1943, pp.
13, 14, 19.

15. (Qa) Oral report from Investigator #2, February 5, 1997.
The fact that the duration of the vacation was two weeks is
from (Qa) Oral report of Investigator #3, February 18, 1997.
According to (Ra) Oral report from Dr. K., March 29, 1997,
in a later interview Freda told Dr. K. that she was no
longer sure that she was away on vacation at the time of my
hospitalization. Instead, as a college student, she may have
been absorbed in her studies and temporarily out of touch
with my parents. But she still affirmed that she had been
told nothing about "that hospital experience" beyond the
bare mention of the fact that I had been in the hospital.
(Ra) Oral Report from Dr. K., February 12, 1998, and (Rb)
Written Information Confirmed by Dr. K., item #1, repeat
this same information, but give May 8, 1997 as the date on
which Dr. K. obtained the information from Freda. Note that
I have a record of receiving this information from Dr. K.
on March 29, 1997. So either Freda gave Dr. K. the same
information twice in different interviews, or else I
inadvertently wrote "March 29" for "May 29" when I dated
the information, or else Dr. K. made an error about the
date.

    In any case, the most important parts of the foregoing
information have been confirmed in writing by
Investigator #2. (Qc) Written Reports by Investigator #2, p.
1: "Freda Tuominen was away on vacation when Ted was
hospitalized as an infant. Upon her return she heard that
Ted had been in the hospital but heard nothing about it
\[sic\] the hospitalization until she read about it in the
media."

16. (Bc) Baby Book, p. 74.

17. Same, p. 85.

18. (Ea) Med Records of TJK, U. Chi., June 23, 1942, p. 7;
September 15, 1942, p. 8.

19. (Bc) Baby Book, p. 113.

20. (Ea) Med Records of TJK, U. Chi., April 6, 1943, p. 12.

21. Same, June 27, 1944, p. 26.

22. Same, April 29, 1944, p. 25.

23. The May 18, 1943 examination is reported in (Bc) Baby
Book, p. 66, but not in the medical records, from which a
page appears to be missing. The other four examinations are
recorded in (Ea) Med Records of TJK, U. Chi., June 13, 1943
and October 19, 1943, p. 23; January 11 and 18, 1944, p. 24.
The "7/13/43" examination reported in (Bc) Baby Book, p.
66, is an error on the part of my mother. It should be
6/13/43, as is shown by the fact that next to 7/13/43, my
mother has the notation "smallpox vaccination," and the
medical records report the vaccination on June 13, 1943.

24. (Bc) Baby Book, p. 122.

25. (Ea) Med Records of TJK, U. Chi., June 13, 1943, p. 23;
April 4, 1945, p. 26; January 4, 1946, p. 27; April 10,
1946, p. 29; October 16, 1947, p. 33; December 8,
1947, p. 34.

26. (Hb) *Washington Post*, June 16, 1996, p. A20.

27. (Bc) Baby Book, p. 76.

28. Same, p. 122.

29. Same, p. 107.

30. Same, p. 114.

31. (La) Schaffer and Callender, "Psychologic Effects of
Hospitalization," *Pediatrics*, October, 1959. This study
considered only babies who were not being breast-fed at the
time they entered the hospital. I fitted into this group
since, by the age of nine months, I was no longer being
breast-fed. See (Bc) Baby Book, p. 104.

32. (Hb) *Washington Post*, June 16, 1996, p. A20. The three
dots appear in the excerpt as printed in the *Post*.

33. My mother first wrote in the Baby Book that I was
"Perfectly healthy but quite and unresponsive." She then
crossed out the "e" at the end of "quite" and inserted
an "e" between the "i" and the "t" to make the word
"quiet." My attorneys Judy Clarke and Quin Denvir examined
the original of the Baby Book (in the possession of the
FBI) and confirmed that the correction appeared to have been
made with the same ink and the same pen as the rest of the
writing in the Baby Book, so that there was no reason to
doubt its authenticity. Since "quite and unresponsive"
would make no sense, and since the correction was clear and
unmistakable, the alteration of "quiet and unresponsive"
to "quite unresponsive" was not an innocent error but
intentional deception on the part of Kovaleski and Adams.
