# Note

When I wrote my first version of <u>Truth versus Lies</u>
I had not had access to the written reports (Qb
and Qc) of Scharlette Holdman and her investigators. Later,
when I received copies of those reports, I had doubts as to
whether Scharlette and her investigators had accurately
recounted what their interviewees had said, and I also
wondered whether they had manipulated the interviewees in
order to elicit the kinds of statements that the
investigators wanted. But I felt I needed to deal with the
investigators' reports in the book in order to make sure
that no one would think I was suppressing important
information. I therefore rewrote <u>Truth versus Lies</u>
, inserting a good deal of discussion of material
from the investigators' reports.

I now wish I had left most of that material out of the book
altogether, because its reliability is open to so much doubt
that I consider it worthless.

In Appendix 10, written in 1998, I outlined some reasons for
being skeptical about the reports of Scharlette Holdman and
her investigators. A few years later, Scharlette and my
friend, the late Joy Richards, were both involved in the
disposition of my cabin, which had been moved from Montana
to Sacramento and was then in the custody of the Federal
Defenders Office. At that time Scharlette told Joy that the
State of California had claimed the right to take possession
of the cabin. Actually it was not the State of California
but the Federal Government that had claimed the cabin, as
Scharlette should have known. Scharlette never explained
this error on her part; in fact, she never afterward
answered any communication from Joy or from me. Needless to
say, this incident intensified my doubts about Scharlette's
ability to collect and report accurate information.

But there is something else that is much more important. At
several points in <u>Truth versus Lies</u> I
cited a declaration (Da) that my father's old friend, the
late Ralph Meister, had signed at the urging of Scharlette
and her collaborators. Much of the declaration was true, but
some parts were false, and it was not clear how Ralph could
have known even the true information contained in the
declaration. So in July 2005 I sent Ralph a copy of his
declaration and invited him to comment on it. In response he
sent me a signed statement (reproduced below) in which he
repudiated the entire declaration.

Clearly Scharlette and her collaborators manipulated Ralph
Meister into signing a declaration that he would never have
signed if he had been free of improper influence. It
therefore seems very probable that Scharlette and her people
similarly manipulated some of the other individuals whom
they interviewed. Consequently, the reader should disregard
all information in this book that is attributed to
Investigator #2 (Scharlette Holdman), Investigator #3 (Gary
Sowards), Investigator #5 (Charlie Pizarro), or
Investigator #6 (Susan Garvey). The information to be
disregarded includes, among other things, all information
cited from Qb and Qc, since Qc consists entirely of
information provided by Investigator #2, and most of the
information in Qb was provided by Investigator #2,
Investigator #5, Investigator #6, or other investigators
working for Scharlette Holdman.

On the other hand, I have no reason to doubt the accuracy of
the information provided by Investigator #1 (Betsy
Anderson), Investigator #4 (Jackie Tully) or Investigator #7
(Nancy Pemberton), none of whom worked closely with
Schalette.

I ought to rewrite <u>Truth versus Lies</u> to
eliminate all dependence on information reported by
Scharlette Holdman and her collaborators, but for the
foreseeable future I won't have time to do that. So, for the
time being the book must remain in its present from, though
with the foregoing warning to the reader.

Ted Kaczynski

May 15, 2007

## \[Transcription by TJK, 5/16/07\]

March 5, 2006 Sunday

Refutation of Declaration

To Whom it may concern:

On July 18, 2005, Theodore John Kaczynski asked me in a
personal correspondence to reconsider a declaration I made
on February 2, 1997.  This document is written in response
to that request. The information and opinions herein
represent the truth to the best of my knowledge and correct
the declaration that while in fact has been signed by me,
upon re-reading, I now feel strongly misrepresents my
statements and the true meaning of those statements.

So much of the declaration is false statements it is
difficult to separate what is true. Paragraphs 1 through 4
are true.

I strongly object to the indiscriminate and inflammatory use
of the word intellectual which appears 12 times in this
short statement; true intellectual, intellectual subjects,
to be an intellectual, intellectual world, intellectual
image, intellectual thought, intellectual giant, this
"almost from the day he was born" rubbish, intellectual
development, intellectual ideals, again intellectual
development, successful intellectual, intellectual
investment, intellectual achievement, I propose to strike
every use of the word, intellectual. In the declaration, it
is obviously misused and meant to mislead.

Theodore Kaczynski's mother Wanda wanted her sons to be
smart just like every mother wants their children to be
smart and successful in life, to have the things she never
had, just like every mother who has had an especially
difficult life and wants to improve herself and provide an
example for her sons and steer them in the right direction.
After her sons were older, Wanda went to college and became
a school teacher. Her sons both pursued a college education.
Wanda followed a generally accepted method of raising
intelligent children. In my experience with testing
children, many many parents wanted to get their child into
kindergarten or first grade early, as soon as the child
passed intellect barriers. My wife, Stella, had a
friendly competitiveness with Wanda since their oldest
children were born months apart and they compared progress.
My objection is that the declaration portrays Wanda as an
extremist, a neurotic who "seemed to have only an
intellectual (dirty word) investment" in her son, once
again, rubbish. She was a loving and devoted mother and I
never meant otherwise.

In paragraph 7, the first sentence is obviously impossible
and once again, inflammatory. Also, she was not "obsessed
with his intellectual development." In the third sentence,
all mothers record milestones, what is religious about baby
books?

Paragraph 8 is another complete fabrication, total out of
control fabrication. I repeat, the last sentence, "She
seemed to have only an intellectual investment in Teddy
John" is pure mean spirited nonsense.

I totally reject paragraphs 9 and 10. These are not my
words, they sound like a script from a soap opera on
television. In fact, considering knowledge I did have of the
Kaczynski's home life during these years, I could never have
reasonably made the statements in paragraphs 9 and 10, and
if I did state anything similar to what was signed, I now
realize I was being completely biased and unjustly
judgemental. The words "badly injured", "feared social
contact", "social deficiencies", "lost control and verbally
abused", "lied to protect", "intense pressure", are not what
I remember at all. No one but Teddy John could have known
exactly how he was feeling, and the last two sentences are
pure conjecture, more soap opera script. Finally, and most
importantly, I never once felt that the Kaczynski family
needed any sort of counseling and I never recommended they
seek professional help. That fact in itself says more about
their homelife than all the hypothesizing and colored
statements in this faulty declaration.

Paragraph 11 is close to accurate. My wife, Stella Meister
greatly admired Theodore for the manner in which he lived
alone in the mountains. She corresponded with him for many
years and looked up to him as a true aesthete. She more than
I understood what joy and solace Theodore found living in
the mountains. "Protection from social deficiencies", Stella
certainly never ever would have thought that.  "Autonomy in
the absence of other social skills represents salvation."
What great philosopher thought of that one, it does not
apply here.  Unfortunately, the last sentence of the
declaration is just too profound.

In short, I believe that it would be best to refute the
declaration I signed in its entirety, and in the future
think twice before I sign a declaration written by someone
else who may have questionable motives rather than seeking
the truth. I hereby do exactly that. I, Ralph K.  Meister
refute the entire attached declaration that I signed on
February 2, 1997.

Sincerely,

Ralph K. Meister

\[signature: Ralph K. Meister\]

Witness: \[signature: Janice Powell(?)\]

Witness: \[signature: Amy Incendela\]

Date: 3/19/06

# TRUTH versus LIES by Ted Kaczynski\*

"An odd principle of human psychology, well known and
exploited. . .  holds that even the silliest of lies can win
credibility by constant repetition."

--- Stephen Jay Gould \*\*

\* Copyright 1998 by Theodore John Kaczynski

\*\* "The Paradox of the Visibly Irrelevant," Natural
History, Volume 106, Number 11, December 1997/January
1998, p. 12.

# Foreword

Though it's the first part of the book, this foreword is
the last part to be written. Its purpose is only to tie up
some loose ends.

To begin with, while this book contains a great deal of
autobiographical material, it is not an autobiography. At
some later time I hope to tell the real story of my life,
especially of my inner development and the changes in my
outlook that took place over the decades.

Before my arrest I never thought there was anything unusual
about my long-term memory. I knew that I remembered things
more accurately than my parents or my brother did, but that
wasn't saying much. Since my arrest, however, several
members of my defense team have told me that my long-term
memory is unusually good. (See Appendix 11.) This is their
opinion; I am not in a position to prove to the reader that
it is correct. There are a few items in this book for which
I have relied entirely on memory and which someone who is
not locked up would be able to check against documentary
evidence. If anyone should take the trouble to dig up the
relevant documents, I hope I will prove to have been right
with regard to most if not all of these items; but, whether
that turns out to be the case or not, the number of such
items is too small to provide a secure evaluation of my
long-term memory.

However, the point I want to make here is that even if the
reader doubts the accuracy of my memories or my honesty in
reporting them, enough of the material in this book is
supported by documentary evidence and/or corroborating
testimony to establish that media reports about me have been
wildly unreliable, and that in its most important aspects my
account of myself and my family relationships is
substantially correct.

As for my use of names, I almost always use the full names
of persons who have spoken about me to the media. When
referring to persons who have not spoken to the media I
usually give names only in abbreviated form.

Some of the facts and incidents that I recount in this book
will be embarrassing to the persons concerned. However, I
assure the reader that my motive has not been to embarrass
anyone, but to bring out the truth and correct false
impressions, for which purpose it has sometimes been
necessary to demonstrate the unreliability of an informant
or show the factors that may have distorted his reports. If
I had wanted to embarrass people there are other facts I
could have related that would have caused a good deal of
additional embarrassment.
